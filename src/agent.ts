import { auth, firestore, storage } from "firebase";
import _ from "lodash";
import { toJSON } from "./models/BaseModel";
import {
	ContractData,
	ContractFormModel,
	ContractModel,
	ContractState
} from "./models/ContractModel";
import { MessageModel } from "./models/MessageModel";
import { ReviewModel } from "./models/ReviewModel";
import { NonInitializedUser, UserModel } from "./models/User";
import UserStore from "./stores/UserStore";
const uuid = require("uuid");

export enum CollectionKeys {
	USERS = "users",
	CONTRACTS = "contracts",
	CONTRACT_DATA = "contract_data",
	MESSAGES = "messages",
	REVIEWS = "reviews"
}

const FirebaseUtils = {
	getUserRef: (id: string) => {
		return firestore()
			.collection(CollectionKeys.USERS)
			.doc(id);
	},
	getUserPhotoRef: (userId: string) => {
		return storage().ref(`users/${userId}/profilePhoto`);
	},
	getContractDataRef: (id: string) => {
		return firestore()
			.collection(CollectionKeys.CONTRACT_DATA)
			.doc(id);
	},
	getContractRef: (id: string) => {
		return firestore()
			.collection(CollectionKeys.CONTRACTS)
			.doc(id);
	},
	getMessageRef: (id: string) => {
		return firestore()
			.collection(CollectionKeys.MESSAGES)
			.doc(id);
	},
	getReviewRef: (id: string) => {
		return firestore()
			.collection(CollectionKeys.REVIEWS)
			.doc(id);
	}
};

const Auth = {
	current: (): firebase.User | null => auth().currentUser,
	login: async (email: string, password: string) =>
		auth().signInWithEmailAndPassword(email, password),
	register: async (email: string, password: string) => {
		try {
			const userCred: firebase.auth.UserCredential = await auth().createUserWithEmailAndPassword(
				email,
				password
			);
			if (!userCred.user || !userCred.user.uid) {
				throw new Error("Could not retrieve a a unique user id, login failed");
			}
			await firestore()
				.collection(CollectionKeys.USERS)
				.doc(userCred.user.uid)
				.set(toJSON(new NonInitializedUser(userCred.user.uid)));
			return userCred.user.uid;
		} catch (e) {
			console.warn("Trouble registering user", e);
			throw e;
		}
	},
	signOut: () => auth().signOut(),
	save: () => {}
};

const User = {
	fetchUserForId: async (id: string): Promise<UserModel> => {
		try {
			const snapShot: firebase.firestore.DocumentSnapshot = await firestore()
				.collection(CollectionKeys.USERS)
				.doc(id)
				.get();
			const data = snapShot.data();
			if (!data) {
				throw Error(`Could not fetch user data for id: ${id}`);
			}
			// TODO: Probably should do some validations here
			return data as UserModel;
		} catch (e) {
			console.warn("Error getting document snapshot for user", e);
			throw e;
		}
	},
	fetchUserCollection: async (): Promise<UserModel[]> => {
		const snapShot: firebase.firestore.QuerySnapshot = await firestore()
			.collection(CollectionKeys.USERS)
			.get();
		return _.map(snapShot.docs, doc => doc.data()) as UserModel[];
	},
	updateUserModel: async (userModel: UserModel) => {
		return await FirebaseUtils.getUserRef(userModel.id).update(userModel);
	},
	uploadPhotoForUser: (photo: File, userId: string): storage.UploadTask => {
		return FirebaseUtils.getUserPhotoRef(userId).put(photo);
	}
};

const Contract = {
	fetchContractForId: async (contractId: string): Promise<any> => {
		const contract = await FirebaseUtils.getContractRef(contractId).get();
		return Promise.resolve(contract.data());
	},
	updateContract: async (contractId: string, contractModel: ContractModel) => {
		return await FirebaseUtils.getContractRef(contractId).update(contractModel);
	},
	updateContractWithNewRevision: async (
		contractId: string,
		contractRevision: ContractFormModel
	) => {
		return firestore().runTransaction(async transaction => {
			const contractRef = FirebaseUtils.getContractRef(contractId);

			const contractDoc = await transaction.get(contractRef);
			if (!contractDoc.exists) {
				throw Error(
					"Attempting to create a revision for contract that does not exist"
				);
			}

			const oldContract = contractDoc.data();
			if (!oldContract) throw Error("Could not get contract data");

			const newContractData: ContractData = {
				...contractRevision,
				created_at: Date.now().toString(),
				updated_at: Date.now().toString()
			};

			const newRevisionId = uuid();
			transaction.update(contractRef, {
				latest_contract_data_id: newRevisionId,
				contract_data_ids: firestore.FieldValue.arrayUnion(newRevisionId),
				contract_revision_map: _.extend(oldContract.contract_revision_map, {
					[newRevisionId]: newContractData
				}),
				updated_at: Date.now().toString()
			});
			return newContractData;
		});
	},
	fetchContractsForId: async (contractIds: string[]): Promise<any> => {
		try {
			const collection: firebase.firestore.QuerySnapshot = await firestore()
				.collection(CollectionKeys.CONTRACTS)
				.get();

			// TODO: This filtering is really wierd find a way to filter on server ???
			const resultContracts = _.map(collection.docs, doc => {
				if (_.includes(contractIds, doc.id)) {
					return doc.data();
				}
			});
			return resultContracts;
		} catch (e) {
			throw e;
		}
	},
	fetchContracts: async (): Promise<any> => {
		const snapShot: firebase.firestore.QuerySnapshot = await firestore()
			.collection(CollectionKeys.CONTRACTS)
			.get();
		return _.map(snapShot.docs, doc => doc.data());
	},
	createNewContract: async (
		contractData: ContractFormModel,
		recipientId: string,
		creatorId: string
	): Promise<string> => {
		const newContractUid = uuid();
		const contractDataUid: string = uuid();
		await firestore()
			.runTransaction(async transaction => {
				const newContractRef = FirebaseUtils.getContractRef(newContractUid);
				const creatorRef = FirebaseUtils.getUserRef(creatorId);
				const recipientRef = FirebaseUtils.getUserRef(recipientId);

				type DocumentSnapshot = firebase.firestore.DocumentSnapshot;
				const [creatorDoc, recipientDoc, newContractDoc]: [
					DocumentSnapshot,
					DocumentSnapshot,
					DocumentSnapshot
				] = await [
					await transaction.get(creatorRef),
					await transaction.get(recipientRef),
					await transaction.get(newContractRef)
				];

				if (!creatorDoc.exists || !recipientDoc.exists) {
					const errorMessage =
						"Attempted to creator a contract for non existent users";
					console.warn(errorMessage);
					throw Error(errorMessage);
				}

				if (newContractDoc.exists) {
					const errorMessage =
						"Attempted to created contract with already existing id";
					console.warn(errorMessage);
					throw Error(errorMessage);
				}

				// TODO: Need to add a validation step before submiting transaction
				const newContractData: ContractModel = {
					id: newContractUid,
					created_at: Date.now().toString(),
					updated_at: Date.now().toString(),
					contract_revision_map: {
						[contractDataUid]: _.extend(contractData, {
							created_at: Date.now().toString(),
							updated_at: Date.now().toString()
						} as ContractData)
					},
					contract_data_ids: [contractDataUid],
					latest_contract_data_id: contractDataUid,
					reviews: [],
					messages: [],
					state: ContractState.PROPOSAL,
					creatorId: creatorId,
					recipientId: recipientId,
					creatorRef: creatorRef,
					recipientRef: recipientRef,
					stub: "?"
				};

				const creatorData = creatorDoc.data();
				const recipientData = recipientDoc.data();
				if (_.isNil(creatorData) || _.isNil(recipientData)) {
					throw Error("Something when wrong getting current user data");
				}

				transaction.set(newContractRef, newContractData);

				transaction.update(creatorRef, {
					contractRefs: _.concat(creatorData.contractRefs, newContractRef),
					contracts: _.concat(creatorData.contracts, newContractUid)
				});
				transaction.update(recipientRef, {
					contractRefs: _.concat(recipientData.contractRefs, newContractRef),
					contracts: _.concat(recipientData.contracts, newContractUid)
				});
			})
			.catch(e => {
				console.error(e);
				throw e;
			});
		return newContractUid;
	}
};

const Review = {
	getReview: async (reviewId: string): Promise<ReviewModel | null> => {
		const reviewObject = await FirebaseUtils.getReviewRef(reviewId).get();
		if (!reviewObject.exists) {
			console.error("This review does not exist");
			return null;
		}
		return reviewObject.data() as ReviewModel;
	},
	createNewReviewForContract: async (reviewModel: ReviewModel) => {
		if (!reviewModel.contractId) {
			throw Error(
				"Review Must have a contractid to associate it with a new contract"
			);
		}
		const contractId: string = reviewModel.contractId as string;
		await firestore().runTransaction(async transaction => {
			const contractRef = FirebaseUtils.getContractRef(contractId);
			const fromUserRef = FirebaseUtils.getUserRef(reviewModel.fromUser);
			const aboutUserRef = FirebaseUtils.getUserRef(reviewModel.aboutUser);

			let oldContractModel = (await contractRef.get()).data();
			let fromUserData = (await fromUserRef.get()).data();
			let aboutUserData = (await aboutUserRef.get()).data();

			if (!oldContractModel || !fromUserData || !aboutUserData) {
				throw Error("could not find contract to associate message with");
			}

			// Update contract with new message
			transaction.update(contractRef, {
				reviews: _.concat(oldContractModel.reviews || [], reviewModel.id)
			});
			transaction.update(fromUserRef, {
				reviews: _.concat(fromUserData.reviews || [], reviewModel.id)
			});
			transaction.update(aboutUserRef, {
				reviews: _.concat(aboutUserData.reviews || [], reviewModel.id)
			});
			transaction.set(FirebaseUtils.getReviewRef(reviewModel.id), reviewModel);
		});
	}
};

const Message = {
	getMessagesByIdList: async (messageIds: string[]): Promise<any> => {
		const messageMap = new Map<string, MessageModel>();
		const asyncFuncs: Promise<any>[] = [];
		if (!messageIds) {
			return;
		}
		messageIds.forEach(id => {
			asyncFuncs.push(
				firestore()
					.collection(CollectionKeys.MESSAGES)
					.doc(id)
					.get()
			);
		});
		const results: firestore.DocumentSnapshot[] = (await Promise.all(
			asyncFuncs
		)) as any;
		return results
			.map(message => message.data())
			.filter(item => !_.isNil(item));
	},
	createNewMessageForContract: async (message: MessageModel) => {
		if (!message.contractId) {
			throw Error(
				"Message Must have a contractid to associate it with a new contract"
			);
		}
		const contractId: string = message.contractId as string;
		firestore().runTransaction(async transaction => {
			const contractRef = FirebaseUtils.getContractRef(contractId);
			let oldContractModel = (await contractRef.get()).data();

			if (!oldContractModel) {
				throw Error("could not find contract to associate message with");
			}

			// Update contract with new message
			transaction.update(contractRef, {
				messages: _.concat(oldContractModel.messages || [], message.id)
			});
			transaction.set(FirebaseUtils.getMessageRef(message.id), message);
		});
	}
};

const ContractListener = {
	attachListenerForContractId: (id: string, callback: () => void) => {
		return FirebaseUtils.getContractRef(id).onSnapshot(doc => {
			callback();
		});
	}
};

const UserListener = {
	attachCurrentUserListener: (id: string) => {
		return FirebaseUtils.getUserRef(id).onSnapshot(doc => {
			UserStore.updateCurrentLoggedInUser(doc.data() as UserModel);
		});
	}
};

const agent = {
	auth: Auth,
	user: User,
	contract: Contract,
	message: Message,
	review: Review,
	listeners: {
		user: UserListener,
		contract: ContractListener
	}
};

export default agent;
