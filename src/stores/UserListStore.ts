import { action, computed, observable, runInAction } from "mobx";
import _ from "lodash";
import { ChangeEvent } from "react";
import agent from "../agent";
import { UserModel } from "../models/User";
import CommonStore from "./CommonStore";
import Fuse from "fuse.js";

class UserListStore {
	@observable
	loadingUserList: boolean = false;
	@observable
	users: UserModel[] | null = null;
	@observable
	loadUserError: any;

	@observable
	searchTerm: string | null = null;

	private fuse: any = new Fuse([], {
		keys: [
			{
				name: "artistName",
				weight: 0.7
			},
			{
				name: "firstName",
				weight: 0.15
			},
			{
				name: "lastName",
				weight: 0.15
			}
		]
	});

	@action
	resetSearchTerm = () => {
		this.searchTerm = null;
	};

	@action
	onSearchTermChange = (event: ChangeEvent<HTMLInputElement>) => {
		this.searchTerm = event.target.value.toLowerCase();
	};

	@computed
	get filteredList(): UserModel[] {
		if (!this.users) return [];
		if (_.isEmpty(this.searchTerm) || this.searchTerm === null)
			return this.users;
		return this.fuse.search(this.searchTerm);
		// const searchString = this.searchTerm.replace(" ", "");

		// return this.users.filter(user => {
		// 	const artistName = user.artistName ? user.artistName.toLowerCase() : "";
		// 	const firstName = user.firstName.toLowerCase();
		// 	const lastName = user.lastName.toLowerCase();
		// 	const searchIndex = artistName + firstName + lastName;
		// 	return searchIndex.includes(searchString);
		// });
	}

	@action
	async getUsers() {
		this.loadingUserList = true;
		this.loadUserError = null;
		try {
			const userModels: UserModel[] = await agent.user.fetchUserCollection();
			runInAction(() => {
				this.users = userModels.filter(
					item => item.uid !== CommonStore.token && item.isProfileSetupComplete
				);
				this.fuse.setCollection(this.users);
			});
		} catch (e) {
			this.loadUserError = e;
			console.warn(e);
			return;
		} finally {
			this.loadingUserList = true;
		}
	}
}

export { UserListStore as UserListStoreType };
export default new UserListStore();
