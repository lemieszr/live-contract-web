import _ from "lodash";
import { action, computed, observable, runInAction } from "mobx";
import agent from "../agent";
import { ContractModel } from "../models/ContractModel";
import { MessageModel } from "../models/MessageModel";
import { UserModel } from "../models/User";
import UserStore from "./UserStore";
const uuid = require("uuid");

export enum MessageEditErrors {
	NO_TEXT = "message-edit/no-text"
}

class MessageEditStore {
	@observable
	errors: any = {};
	@observable
	creatingMessage: boolean = false;

	@action
	createNewMessageForChatWindow = async (
		message: any,
		relatedContractId: string
	) => {
		this.creatingMessage = true;
		try {
			const currentUser = UserStore.currentLoggedInUser;
			if (!currentUser) {
				throw Error("We need a valid user");
			}
			const newMessage = {
				id: uuid(),
				contractId: relatedContractId,
				messageText: message.data.text,
				creatorId: currentUser.id
			};
			await agent.message.createNewMessageForContract(newMessage);
			this.messageList.push(newMessage);
		} catch (e) {
		} finally {
			this.creatingMessage = false;
		}
	};

	@computed
	get messagesForWidget() {
		if (UserStore.currentLoggedInUser) {
			const { id } = UserStore.currentLoggedInUser;
			return this.messageList.map(message => ({
				author: message.creatorId === id ? "me" : "them",
				type: "text",
				data: {
					text: message.messageText
				}
			}));
		}
		return [];
	}

	@observable
	messageList: MessageModel[] = [];
	@observable
	loadingMessages: boolean = false;
	@observable
	loadMessagesError: any = null;
	@observable
	messageToUserMap = new Map<string, UserModel | null>();

	userNameForMessageId(id: string) {
		const user = this.messageToUserMap.get(id);
		if (!user) return id;
		return `${user.firstName} ${user.lastName}`;
	}

	@observable
	listenerUnsubscribe: () => void = () => {};

	@action
	attachListener(id: string) {
		this.listenerUnsubscribe = agent.listeners.contract.attachListenerForContractId(
			id,
			() => runInAction(() => this.getMessagesById(id))
		);
	}

	@action
	async getMessagesById(contractId: string) {
		this.loadingMessages = true;
		try {
			const contract: ContractModel = await agent.contract.fetchContractForId(
				contractId
			);
			if (!contract.messages || _.isEmpty(contract.messages)) {
				this.messageList = [];
				return;
			}
			const messages: MessageModel[] = await agent.message.getMessagesByIdList(
				contract.messages
			);
			messages.forEach(async message => {
				if (!message.creatorId) {
					this.messageToUserMap.set(message.id, null);
					return;
				}
				const user = await UserStore.pullUser(message.creatorId as string);
				runInAction(() => {
					this.messageToUserMap.set(message.id, user || null);
				});
			});
			runInAction(() => {
				this.messageList = messages;
			});
		} catch (e) {
			this.loadMessagesError = e;
			throw e;
		} finally {
			this.loadingMessages = false;
		}
	}

	@action
	reset() {
		this.messageList = [];
		this.listenerUnsubscribe();
	}
}

export { MessageEditStore as MessageEditStoreType };
export default new MessageEditStore();
