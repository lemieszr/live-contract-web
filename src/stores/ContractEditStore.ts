import { observable, action, set } from "mobx";
import {
	ContractFormModel,
	ContractData,
	ContractModel
} from "../models/ContractModel";
import agent from "../agent";
import _ from "lodash";
import ContractsStore from "./ContractsStore";

class ContractEditStore {
	@observable
	currentContractId: string | null = null;

	@observable
	contractCurrentModel: ContractData | null = null;

	@observable
	updatingContract: boolean = false;

	public defaultForm = {
		firstName: "",
		lastName: "",
		artistName: "",
		compensation: "",
		date: "",
		notes: "",
		venue: "",
		venueAddress: "",
		setTime: "",
		setLength: "",
		eventName: "",

		flight: false,
		rider: false,
		hotel: false
	};

	@observable
	public contractFormModel: ContractFormModel = this.defaultForm;

	@action
	public updateContractField(field: string, value: any) {
		set(this.contractFormModel, field, value);
	}

	@action
	public async init(contractId: string, currentContractModel?: ContractData) {
		this.updatingContract = false;
		this.currentContractId = contractId;

		if (!currentContractModel) {
			try {
				const contract: ContractModel = await agent.contract.fetchContractForId(
					contractId
				);
				const currentContractRevision: ContractData =
					contract.contract_revision_map[contract.latest_contract_data_id];
				this.contractCurrentModel = currentContractRevision;
			} catch (e) {
				console.error(e);
				throw new Error(e);
			} finally {
			}
		} else {
			this.contractCurrentModel = { ...currentContractModel };
			this.contractFormModel = { ...currentContractModel };
		}
	}

	@action
	public async updateContract() {
		if (!this.contractFormModel || !this.currentContractId) {
			throw Error("Can not update contract without form modal or contract id");
		}

		if (_.isEqual(this.contractFormModel, this.contractCurrentModel)) {
			console.log("Contracts are equal no update");
			return;
		}

		this.updatingContract = true;
		try {
			const newData = await agent.contract.updateContractWithNewRevision(
				this.currentContractId,
				this.contractFormModel
			);
			ContractsStore.updateContractData(newData);
		} finally {
			this.updatingContract = false;
		}
	}

	@action
	public reset() {
		this.updatingContract = false;
		this.contractFormModel = this.defaultForm;
		this.currentContractId = null;
		this.contractCurrentModel = null;
	}
}

export { ContractEditStore as ContractEditStoreType };
export default new ContractEditStore();
