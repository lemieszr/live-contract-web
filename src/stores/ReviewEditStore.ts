import { action, observable } from "mobx";
import agent from "../agent";
import { ContractModel } from "../models/ContractModel";
import UserStore from "./UserStore";
import { ReviewModel } from "../models/ReviewModel";
const uuid = require("uuid");

class ReviewEditStore {
	@observable
	ratingValue: number = 0;
	@observable
	reviewText: string = "";
	@observable
	reviewId: string = "";
	@observable
	contractId: string = "";
	@observable
	contractModel: ContractModel | null = null;
	@observable
	reviewSubmitted: boolean = false;

	@action
	async initReview(contractId: string) {
		this.reviewId = uuid();
		this.contractId = contractId;
		try {
			const contractModel: ContractModel = await agent.contract.fetchContractForId(
				contractId
			);

			const currentUser = UserStore.currentUser;
			if (!currentUser) return;
			const currentUserId = currentUser.id;

			if (contractModel && contractModel.reviews.length > 0) {
				contractModel.reviews.forEach(async reviewId => {
					const review: ReviewModel | null = await agent.review.getReview(
						reviewId
					);
					if (review && review.fromUser === currentUserId) {
						this.reviewSubmitted = true;
					}
				});
			}
			this.contractModel = contractModel;
		} catch (e) {
			console.error(e);
			throw Error("Could not find model for contract id");
		}
	}

	@action
	reset() {
		this.reviewId = "";
		this.ratingValue = 0;
		this.reviewId = "";
		this.contractModel = null;
	}

	@action
	setRatingValue(value: number) {
		this.ratingValue = value;
	}

	@action
	setReviewText(text: string) {
		this.reviewText = text;
	}

	@action
	submitReview = async () => {
		if (!this.contractModel) return;

		const currentUser = UserStore.currentUser;
		if (!currentUser) return;
		const currentUserId = currentUser.id;
		try {
			await agent.review.createNewReviewForContract({
				contractId: this.contractId,
				id: this.reviewId,
				reviewText: this.reviewText,
				fromUser: currentUserId,
				aboutUser:
					currentUserId === this.contractModel.recipientId
						? this.contractModel.creatorId
						: this.contractModel.recipientId,
				userId: "n/a",
				value: this.ratingValue
			});
			this.reviewSubmitted = true;
		} catch (e) {
			console.log(e);
		}
	};
}

export default new ReviewEditStore();
export { ReviewEditStore as ReviewEditStoreType };
