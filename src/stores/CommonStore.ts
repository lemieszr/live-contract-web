import { action, computed, observable, reaction } from "mobx";
import ContractsStore from "./ContractsStore";

class CommonStore {
	@observable
	appName = "Conduit";
	@observable
	token = window.localStorage.getItem("userToken");
	@observable
	appLoaded = false;

	constructor() {
		reaction(
			() => this.token,
			(token: any) => {
				if (token) {
					window.localStorage.setItem("userToken", token);
				} else {
					window.localStorage.removeItem("userToken");
				}
			}
		);
	}

	@action
	setToken(token: any) {
		this.token = token;
	}

	@action
	setAppLoaded() {
		this.appLoaded = true;
	}

	@computed
	get appScrollDisabled() {
		return ContractsStore.modalOpen === true;
	}
}

export { CommonStore };

export default new CommonStore();
