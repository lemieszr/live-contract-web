import { observable, action, runInAction } from "mobx";
import ProfileCreatePage from "../features/ProfileCreatePage/ProfileCreatePage";
import UserStore from "./UserStore";
import { UserModel } from "../models/User";
import agent from "../agent";
import { ObservableObjectAdministration } from "mobx/lib/internal";
import * as Sentry from "@sentry/browser";

export interface CreateProfileStoreValue {
	firstName: string;
	lastName: string;
	photoFile: File | null;
	artistName: string;
}

export enum CreateProfileErrors {
	INVALID_FIELD = "INVALID_FIELDS",
	NETWORK_FAILURE = "NETWORK_FAILURE"
}

class ProfileCreateStore {
	@observable
	public isLoading: boolean = false;

	@observable
	public error: any = null;

	@observable
	public values: CreateProfileStoreValue = {
		firstName: "",
		lastName: "",
		photoFile: null,
		artistName: ""
	};

	@action
	public reset() {
		this.values = {
			firstName: "",
			lastName: "",
			photoFile: null,
			artistName: ""
		};
	}

	@action
	firstNameHandler = (value: string) => (this.values.firstName = value);

	@action
	lastNameHandler = (value: string) => (this.values.lastName = value);

	@action
	photoUrlHandler = (value: File) => (this.values.photoFile = value);

	@action
	artistName = (value: string) => (this.values.artistName = value);

	@action
	public updateUserProfile = async () => {
		this.isLoading = true;
		try {
			const userModel: UserModel | null = UserStore.currentLoggedInUser;
			if (!userModel) {
				return;
			}

			if (this.values.firstName == "" || this.values.lastName == "") {
				runInAction(() => {
					this.error = CreateProfileErrors.INVALID_FIELD;
				});
				return;
			}

			if (this.values.photoFile == null) {
				throw new Error("must upload file");
			}

			const snapshot = await agent.user.uploadPhotoForUser(
				this.values.photoFile,
				userModel.id
			);

			const downloadUrl = await snapshot.ref.getDownloadURL();

			const newUserModel = {
				...userModel,
				firstName: this.values.firstName,
				lastName: this.values.lastName,
				isProfileSetupComplete: true,
				profileImageUrl: downloadUrl || "",
				artistName: this.values.artistName
			};
			await [
				agent.user.updateUserModel(newUserModel),
				UserStore.updateCurrentLoggedInUser(newUserModel)
			];
		} catch (e) {
			console.log("Failed to create new profile", e);
			const userModel: UserModel | null = UserStore.currentLoggedInUser;

			if (userModel) {
				Sentry.withScope(scope => {
					scope.setUser(userModel);
					Sentry.captureException(e);
				});
			}

			this.error = CreateProfileErrors.NETWORK_FAILURE;
			Sentry;
			return;
		} finally {
			this.isLoading = false;
		}
	};
}

export { ProfileCreateStore as ProfileCreateStoreType };
export default new ProfileCreateStore();
