import { observable, computed, action, runInAction } from "mobx";
import agent from "../agent";
import UserStore from "./UserStore";
import CommonStore from "./CommonStore";

export enum AuthErrorCode {
	WRONG_PASSWORD = "auth/wrong-password",
	EMAIL_TAKEN = "auth/email-already-in-use",
	USER_NOT_FOUND = "auth/user-not-found",
	PASSWORD_CONFIRM = "auth/password-confirm",
	FIREBASE_DELETED = "auth/app-deleted",
	NOT_AUTHORIZED = "auth/app-not-authorized",
	ARGUMENT_ERROR = "auth/argument-error",
	INVALID_API_KEY = "auth/invalid-api-key",
	INVALID_USER_TOKER = "auth/invalid-user-token",
	NETWORK_REQUEST_FAILED = "auth/network-request-failed",
	OPERATION_NOT_ALLOWED = "auth/operation-not-allowed",
	REQUIRES_RECENT_LOGIN = "auth/requires-recent-login",
	TOO_MANY_REQUESTS = "auth/too-many-requests",
	UNAUTHORIZED_DOMAIN = "auth/unauthorized-domain",
	USER_DISABLED = "auth/user-disabled",
	USER_TOKEN_EXPIRED = "auth/user-token-expired",
	WEB_STORAGE_UNSUPPORTED = "auth/web-storage-unsupported",
	EXPIRED_ACTION_CODE = "auth/expired-action-code",
	INVALID_ACTION_CODE = "auth/invalid-action-code",
	WEAK_PASSWORD = "auth/weak-password",
	INVALID_EMAIL = "auth/invalid-email",
	EMAIL_OPERATION_NOT_ALLOWED = "auth/operation-not-allowed",
	MISSING_CONTINUE_URL = "auth/missing-continue-uri",
	INVALID_CONTINUE_URL = "auth/invalid-continue-uri",
	UNAUTHORIZED_CONTINUE_URL = "auth/unauthorized-continue-uri",
	INVALID_DYNAMIC_LINK_DOMAIN = "auth/invalid-dynamic-link-domain",
	INVALID_PERSISTENCE_TYPE = "auth/invalid-persistence-type",
	UNSUPPORTED_PERSISTENCE_TYPE = "auth/unsupported-persistence-type",
	ACCOUNT_ALREADY_EXISTS_WITH_OTHER_CREDENTIALS = "auth/account-exists-with-different-credential",
	INVALID_VERIFICATION_CODE = "auth/invalid-verification-code",
	INVALID_VERIFICATION_ID = "auth/invalid-verification-id",
	CUSTOM_TOKEN_MISMATCH = "auth/custom-token-mismatch",
	INVALID_CUSTOM_TOKEN = "auth/invalid-custom-token"
}

export interface AuthStoreValue {
	username: string;
	email: string;
	password: string;
	passwordConfirm: string;
}

class AuthStore {
	@observable
	inProgress: boolean = false;
	@observable
	errors: any = undefined;
	@observable
	values: AuthStoreValue = {
		username: "",
		email: "",
		password: "",
		passwordConfirm: ""
	};
	@observable
	invalidEmail: boolean = false;

	@action
	setUsername(username: string) {
		this.values.username = username;
	}

	@action
	setEmail(email: string) {
		this.values.email = email;
	}

	@action
	setPassword(password: string) {
		this.values.password = password;
	}

	@action
	setPasswordConfirm(password: string) {
		this.values.passwordConfirm = password;
	}

	@action
	reset() {
		this.values.username = "";
		this.values.email = "";
		this.values.password = "";
		this.values.passwordConfirm = "";
	}

	@action
	async login() {
		this.inProgress = true;
		this.errors = undefined;

		try {
			const data: firebase.auth.UserCredential = await agent.auth.login(
				this.values.email,
				this.values.password
			);
			if (!data.user) {
				throw Error("Could not find user data");
			}
			await UserStore.pullUser(data.user.uid, true);
		} catch (e) {
			this.errors = e.code;
			console.log(e);
		} finally {
			this.inProgress = false;
		}
	}

	@observable
	private passwordInvalid: boolean = false;

	@action
	async signupNewUser() {
		this.inProgress = true;
		this.errors = undefined;

		try {
			if (this.values.password !== this.values.passwordConfirm) {
				this.passwordInvalid = true;
				this.errors = AuthErrorCode.PASSWORD_CONFIRM;
				return;
			}

			const userId = await agent.auth.register(
				this.values.email,
				this.values.password
			);
			CommonStore.setToken(userId);
			await UserStore.pullUser(userId, true);
		} catch (e) {
			runInAction(() => {
				this.errors = e.code;
			});
			console.log(e);
		} finally {
			this.inProgress = false;
		}
	}
}

export { AuthStore };
export default new AuthStore();
