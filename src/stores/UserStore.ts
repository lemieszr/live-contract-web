import { action, computed, observable, runInAction } from "mobx";
import { RouterStore } from "..";
import agent from "../agent";
import { ReviewCarouselCardsProps } from "../components/UserProfile/UserProfile";
import { ReviewModel } from "../models/ReviewModel";
import { UserModel } from "../models/User";
import Routes from "../utility/Routes";
import CommonStore from "./CommonStore";

class UserStore {
	@observable
	loadingUser: boolean = false;
	@observable
	currentUser: UserModel | null = null;
	@observable
	loadUserError: any;

	@observable
	users: { [id: string]: UserModel } = {};

	@observable
	currentLoggedInUser: UserModel | null = null;

	@action
	updateCurrentLoggedInUser(userModel: UserModel | null) {
		this.currentLoggedInUser = userModel;
		CommonStore.setToken(userModel ? userModel.uid : null);

		if (userModel && !userModel.isProfileSetupComplete) {
			RouterStore.replace(Routes.PROFILE_CREATE.path);
		}
	}

	@action
	async pullUser(id: string, isLoginAction: boolean = false) {
		this.loadingUser = true;
		try {
			const user: UserModel = await agent.user.fetchUserForId(id);
			runInAction(() => {
				this.users[user.id] = user;
				if (isLoginAction) {
					this.currentLoggedInUser = user;
					CommonStore.setToken(user.uid);
				}
				this.currentUser = user;
			});
			return user;
		} catch (e) {
			this.loadUserError = e;
			console.warn("Error fetching user", e);
			return;
		} finally {
			this.loadingUser = false;
		}
	}

	@observable
	public currentUserProfile: UserModel | null = null;
	@observable
	public userProfileReviews: ReviewModel[] = [];

	@computed
	get reviewCarouselCards(): ReviewCarouselCardsProps[] {
		return this.userProfileReviews.map(review => {
			return {
				text: review.reviewText,
				rating: review.value,
				reviewer: this.users[review.fromUser]
					? this.users[review.fromUser].artistName
					: review.fromUser
			};
		});
	}

	@action
	public resetProfile() {
		this.currentUserProfile = null;
		this.userProfileReviews = [];
	}

	@action
	async pullUserProfile(id: string) {
		this.loadingUser = true;
		try {
			const user: UserModel = await agent.user.fetchUserForId(id);
			runInAction(() => {
				this.users[user.id] = user;
				this.currentUserProfile = user;
			});
			runInAction(() => {
				const reviewCollector: ReviewModel[] = [];
				this.userProfileReviews = [];
				user.reviews.map(async reviewId => {
					const reviewData = await agent.review.getReview(reviewId);
					if (reviewData === null) return;
					const user = await agent.user.fetchUserForId(reviewData.fromUser);
					this.users[user.id] = user;
					this.userProfileReviews.push(reviewData);
				});
			});
			return user;
		} catch (e) {
			this.loadUserError = e;
			console.warn("Error fetching user", e);
			return;
		} finally {
			this.loadingUser = false;
		}
	}
}

export { UserStore };
export { UserStore as UserStoreType };
export default new UserStore();
