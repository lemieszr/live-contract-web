import { observable, action, runInAction, computed, set } from "mobx";
import _ from "lodash";
import {
	ContractModel,
	ContractFormModel,
	ContractState,
	ContractData
} from "../models/ContractModel";
import agent from "../agent";
import { UserModel } from "../models/User";
import UserStore from "./UserStore";
import Routes, { buildProfilePath } from "../utility/Routes";
import { MessageModel } from "../models/MessageModel";
import moment from "moment";
export interface SortedContracts {
	receivedContracts: ContractModel[];
	sentContracts: ContractModel[];
	acceptedContracts: ContractModel[];
	rejectedContracts: ContractModel[];
}

class ContractsStore {
	@observable
	loadingContracts: boolean = false;

	@observable
	contracts: ContractModel[] = [];

	@computed
	get sortedContracts(): SortedContracts {
		const user = UserStore.currentLoggedInUser;
		if (!user) {
			return {
				acceptedContracts: [],
				receivedContracts: [],
				rejectedContracts: [],
				sentContracts: []
			};
		}
		const result = this.contracts.reduce<SortedContracts>(
			(acc: SortedContracts, contract) => {
				if (contract.state === ContractState.ACCEPTED) {
					acc.acceptedContracts.push(contract);
				} else if (
					contract.state == ContractState.REJECTED ||
					contract.state == ContractState.CANCELED
				) {
					acc.rejectedContracts.push(contract);
				} else if (contract.recipientId == user.id) {
					acc.receivedContracts.push(contract);
				} else if (contract.creatorId == user.id) {
					acc.sentContracts.push(contract);
				}
				return acc;
			},
			{
				sentContracts: [],
				receivedContracts: [],
				acceptedContracts: [],
				rejectedContracts: []
			}
		);
		return result;
	}

	@observable
	loadingContractsError: any;

	@action
	async pullContractsForCurrentUser(user: UserModel) {
		this.loadingContracts = true;
		try {
			const contractModels: ContractModel[] = await agent.contract.fetchContracts();
			const userContractIds: string[] = user.contracts;
			runInAction(() => {
				this.contracts = contractModels.filter(model =>
					userContractIds.includes(model.id)
				);
			});
		} catch (e) {
		} finally {
			this.loadingContracts = false;
		}
	}

	@observable
	loadingDetailPage: boolean = false;
	@observable
	loadDetailPageError: any = null;
	@observable
	detailPageCurrentContract: ContractModel | null = null;
	@observable
	detailPageCreatorModel: UserModel | null = null;
	@observable
	revisionCount: number = -1;

	@observable
	detailPageCurrentContractData: ContractData | null = null;

	@action
	updateContractData(contractData: ContractData) {
		this.detailPageCurrentContractData = contractData;
	}

	@computed
	get isCreator() {
		if (!this.detailPageCurrentContract || !UserStore.currentLoggedInUser) {
			return false;
		}

		return (
			this.detailPageCurrentContract.creatorId ==
			UserStore.currentLoggedInUser.id
		);
	}

	@computed
	get creatorName() {
		if (!this.detailPageCreatorModel || !this.detailPageCurrentContract) {
			return "";
		}

		return `${this.detailPageCreatorModel.firstName} ${
			this.detailPageCreatorModel.lastName
		}`;
	}

	@computed
	get contractExpired() {
		if (!this.detailPageCurrentContractData) return;
		const date = this.detailPageCurrentContractData.date;
		const expiration = moment(date);
		const today = moment.now();
		return expiration.diff(today) < 0;
	}

	@computed
	get creatorProfileLink(): string {
		if (!this.detailPageCreatorModel || !this.detailPageCurrentContract) {
			return "";
		}

		if (this.isCreator) {
			return Routes.HOME.path;
		}

		return buildProfilePath(this.detailPageCreatorModel.id);
	}

	@action
	async getContractForDetailPage(contractId: string) {
		this.loadingDetailPage = true;
		try {
			const contractModel: ContractModel = await agent.contract.fetchContractForId(
				contractId
			);
			const creatorModel:
				| UserModel
				| null
				| undefined = await UserStore.pullUser(contractModel.creatorId);

			if (!creatorModel) {
				throw new Error(
					"Can not load detail page, no creator associated with this contract"
				);
			}

			runInAction(() => {
				this.detailPageCreatorModel = creatorModel;
				this.detailPageCurrentContract = contractModel;
				this.detailPageCurrentContractData =
					contractModel.contract_revision_map[
						contractModel.latest_contract_data_id
					];
				this.revisionCount = contractModel.contract_data_ids.length;
			});
		} catch (e) {
			this.loadDetailPageError = e;
		} finally {
			this.loadingDetailPage = false;
		}
	}

	@action
	async getContracts(contractIds: string[]) {
		this.loadingContracts = true;
		try {
			// const contracts: Contract[] =
		} catch {
		} finally {
			this.loadingContracts = false;
		}
	}

	@observable
	creatingContract: boolean = false;
	@observable
	createContractError: any;
	@observable
	contractRecipientId: string | null = null;
	@observable
	contractCreatorId: string | null = null;
	@observable
	createdContractId: string = "";

	@action setCreator(user: string | null) {
		this.contractCreatorId = user;
	}
	@action setRecipient(user: string | null) {
		this.contractRecipientId = user;
	}

	@action
	async createContract() {
		this.creatingContract = true;
		this.createContractError = null;
		this.modalOpen = false;
		try {
			if (
				_.isNil(this.contractRecipientId) ||
				_.isNil(this.contractCreatorId)
			) {
				throw new Error(
					"Could not create a contract, did not have defined users"
				);
			}

			const newContractUid = await agent.contract.createNewContract(
				this.contractFormModel,
				this.contractRecipientId,
				this.contractCreatorId
			);

			this.createdContractId = newContractUid;

			this.modalOpen = true;

			if (UserStore.currentLoggedInUser) {
				// TODO: This is a hack should have some sort of caching enable here.
				await this.pullContractsForCurrentUser(UserStore.currentLoggedInUser);
			}
		} catch (e) {
			this.createContractError = e;
			console.warn(e);
		} finally {
			this.creatingContract = false;
		}
	}

	@observable
	public loadingCreateContractPage: boolean = false;
	@observable
	public createContractLoadError: any = null;
	@observable
	public recipientModel: UserModel | null | undefined = null;

	public defaultForm = {
		firstName: "",
		lastName: "",
		artistName: "",
		compensation: "",
		date: "",
		notes: "",
		venue: "",
		venueAddress: "",
		setTime: "",
		setLength: "",
		eventName: "",

		flight: false,
		rider: false,
		hotel: false
	};

	@observable
	public contractFormModel: ContractFormModel = this.defaultForm;

	@action
	public updateContractField(field: string, value: any) {
		set(this.contractFormModel, field, value);
	}

	@observable
	public modalOpen: boolean = false;

	@action
	async loadCreateContractsPage(id: string) {
		this.loadingCreateContractPage = true;
		try {
			this.recipientModel = await UserStore.pullUser(id);
			if (!this.recipientModel)
				throw new Error("Could not load contract recipient");
			this.contractFormModel.firstName = this.recipientModel.firstName;
			this.contractFormModel.lastName = this.recipientModel.lastName;
			this.contractFormModel.artistName = this.recipientModel.artistName;
		} catch (e) {
			console.error("Failed to load create contract page", e);
			throw e;
		} finally {
			this.loadingCreateContractPage = false;
		}
	}

	@action
	resetCreateContract() {
		this.creatingContract = false;
		this.createContractError = false;
		this.contractRecipientId = null;
		this.contractCreatorId = null;
		this.modalOpen = false;
		this.createContractLoadError = false;
		this.recipientModel = null;
		this.createdContractId = "";
	}

	@action
	async updateContractStatus(state: ContractState) {
		try {
			if (!this.detailPageCurrentContract) {
				throw new Error("should not be at this point if contract is null");
			}
			const updatedContract = _.clone(this.detailPageCurrentContract);
			updatedContract["state"] = state;
			await agent.contract.updateContract(
				this.detailPageCurrentContract.id,
				updatedContract
			);
			this.detailPageCurrentContract = updatedContract;
		} catch (e) {
			console.error("error", e);
			throw e;
		} finally {
		}
	}
}

export { ContractsStore as ContractsStoreType };
export default new ContractsStore();
