import { observable, action } from "mobx";

class BottomNavStore {
	@observable
	public currentVal: number = 0;

	@action
	public handleChange = (event: any, newValue: number) => {
		this.currentVal = newValue;
	};

	@action
	public setValue = (newValue: number) => {
		this.currentVal = newValue;
	};
}

export { BottomNavStore as BottomNavStoreType };
export default new BottomNavStore();
