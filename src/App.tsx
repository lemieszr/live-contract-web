import * as Sentry from "@sentry/browser";
import classnames from "classnames";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import agent from "./agent";
import "./App.scss";
import Header from "./components/Header/Header";
import LoadingContainer from "./components/LoadingContainer/LoadinContainer";
import RoutePrivate from "./components/Routes/RoutePrivate";
import { UserModel } from "./models/User";
import { CommonStore } from "./stores/CommonStore";
import { UserStore } from "./stores/UserStore";
import Routes from "./utility/Routes";
import "typeface-roboto";
import BottomNav from "./components/BottomNav/BottomNav";

export interface InjectProps {
	userStore: UserStore;
	commonStore: CommonStore;
}

const MainScreen = (props: { token: string; match: any }) => (
	<React.Fragment>
		<Switch>
			<Route
				component={Routes.USER_LIST.component}
				path={`${Routes.MAIN.path}${Routes.USER_LIST.path}`}
			/>
			<Route
				component={Routes.USER_PROFILE.component}
				path={`${Routes.MAIN.path}${Routes.USER_PROFILE.path}`}
			/>
			<Route
				component={Routes.CONTRACTS_LIST.component}
				path={`${Routes.MAIN.path}${Routes.CONTRACTS_LIST.path}`}
			/>
			<Redirect
				from="/main"
				to={`${props.match.url}${Routes.USER_PROFILE.path}`}
			/>
		</Switch>
		<BottomNav />

		{/* // <Footer match={props.match} /> */}
	</React.Fragment>
);

@(withRouter as any)
@inject("userStore", "commonStore", "headerStore")
@observer
export default class App extends Component {
	private commonStore: CommonStore;
	private userStore: UserStore;
	private detachCurrentUserListener: () => void = () => {};

	constructor(props: InjectProps) {
		super(props);
		this.commonStore = props.commonStore;
		this.userStore = props.userStore;
	}

	componentDidCatch(error: any, errorInfo: any) {
		Sentry.withScope(scope => {
			Object.keys(errorInfo).forEach(key => {
				scope.setExtra(key, errorInfo[key]);
			});
			Sentry.captureException(error);
		});
	}

	componentWillMount() {
		const token = this.commonStore.token;
		if (this.commonStore.token) {
			this.userStore
				.pullUser(this.commonStore.token, true)
				.then(() => {
					if (!this.commonStore.token) {
						throw Error("App loaded but no token was set");
					}
					const currentUser: UserModel | null = this.userStore
						.currentLoggedInUser;

					agent.listeners.user.attachCurrentUserListener(this.commonStore
						.token as string);
				})
				.finally(() => this.commonStore.setAppLoaded());
		} else {
			this.commonStore.setAppLoaded();
		}
	}

	componentWillUnmount() {
		this.detachCurrentUserListener();
	}

	render() {
		if (!this.commonStore.appLoaded) {
			return <LoadingContainer />;
		}
		const userStore = this.userStore;
		if (!userStore) {
			console.warn("Could not render app user store not available");
			return null;
		}

		const appClass = classnames("App", {
			"App--scroll-disable": this.commonStore.appScrollDisabled
		});

		return (
			<div className={appClass}>
				<Header showHeader={this.commonStore.token !== null} />
				<div className="App__content">
					<Switch>
						<RoutePrivate
							currentUser={userStore.currentLoggedInUser}
							path={Routes.MAIN.path}
							component={MainScreen}
						/>
						<RoutePrivate
							currentUser={userStore.currentLoggedInUser}
							{...Routes.OTHER_PROFILE}
						/>
						<RoutePrivate
							currentUser={userStore.currentLoggedInUser}
							{...Routes.CREATE_CONTRACT}
						/>
						<RoutePrivate
							currentUser={userStore.currentLoggedInUser}
							{...Routes.CONTRACT_DETAIL}
						/>
						<RoutePrivate
							currentUser={userStore.currentLoggedInUser}
							{...Routes.PROFILE_CREATE}
						/>

						<Route {...Routes.LOGIN} />
						<Route {...Routes.SIGNUP} />

						<Redirect
							from="/"
							to={`${Routes.MAIN.path}${Routes.USER_PROFILE.path}`}
						/>
					</Switch>
				</div>
			</div>
		);
	}
}
