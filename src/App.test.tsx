import React from 'react';
import { withRouter, Switch, Route, Redirect } from "react-router-dom";
import RoutePrivate from "../src/components/Routes/RoutePrivate";
import Routes from "./utility/Routes";
import { inject, observer } from "mobx-react";
import { UserStore } from "../src//stores/UserStore";
import { CommonStore } from "../src//stores/CommonStore";
import Footer from "../src//components/Footer/Footer";
import agent from "../src//agent";
import Header from "../src//components/Header/Header";
import { HeaderStoreType } from "../src//components/Header/HeaderStore";
import { UserModel } from "../src//models/User";
import Button from "../src/components/Buttons/Button";
import Carousel from "../src/components/Carousel/Carousel";
import TextArea from "../src/components/FormComponents/TextArea";
import TextInput from "../src/components/FormComponents/TextArea";
import AuthPage from "../src/features/Auth/AuthPage";
import HomePage from "../src/features/HomePage/HomePage";
import UserListPage from "../src/features/UserListPage/UserListPage";
import ContractsListPage from "../src/features/ContractsListPage/ContractsListPage";
import OtherProfilePage from "../src/features/OtherProfilePage/OtherProfilePage";
import CreateContractPage from "../src/features/CreateContract/CreateContractPage";
import ContractDetailPage from "../src/features/ContractDetail/ContractDetailPage";
import ProfileCreatePage from "../src/features/ProfileCreatePage/ProfileCreatePage";

test('Fake Test', () => {
  expect(true).toBeTruthy();
})

test('Test Routes', () => {
  const testRoutes = {
    LOGIN: {
  		component: AuthPage,
  		path: "/login",
  		exact: true
  	},
  	SIGNUP: {
  		component: AuthPage,
  		path: "/signup",
  		exact: true
  	},
  	HOME: {
  		component: HomePage,
  		path: "/",
  		exact: true
  	},
  	USER_PROFILE: {
  		component: HomePage,
  		path: "/profile/",
  		exact: true
  	},
  	USER_LIST: {
  		component: UserListPage,
  		path: "/users/",
  		exact: true
  	},
  	CONTRACTS_LIST: {
  		component: ContractsListPage,
  		path: "/contracts/",
  		exact: true
  	},
  	OTHER_PROFILE: {
  		component: OtherProfilePage,
  		path: "/user/:id/",
  		exact: true
  	},
  	PROFILE_CREATE: {
  		component: ProfileCreatePage,
  		path: `/profile/create/`,
  		exact: true
  	},
  	MAIN: {
  		path: "/main",
  		exact: true
  	}
  }
  expect(Routes.LOGIN).toEqual(testRoutes.LOGIN);
  expect(Routes.SIGNUP).toEqual(testRoutes.SIGNUP);
  expect(Routes.CONTRACTS_LIST).toEqual(testRoutes.CONTRACTS_LIST);
  expect(Routes.HOME).toEqual(testRoutes.HOME);
  expect(Routes.MAIN).toEqual(testRoutes.MAIN);
  expect(Routes.USER_PROFILE).toEqual(testRoutes.USER_PROFILE);
  expect(Routes.PROFILE_CREATE).toEqual(testRoutes.PROFILE_CREATE);
  expect(Routes.USER_LIST).toEqual(testRoutes.USER_LIST);
  expect(Routes.OTHER_PROFILE).toEqual(testRoutes.OTHER_PROFILE);
})
