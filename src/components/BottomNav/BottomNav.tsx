import React, { useEffect } from "react";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import AppBar from "@material-ui/core/AppBar";
import { RouteComponentProps, withRouter } from "react-router";
import Routes from "../../utility/Routes";
import BottomNavStore from "../../stores/BottomNavStore";
import { observer } from "mobx-react";

@(withRouter as any)
@observer
class BottomNav extends React.Component<any> {
	render() {
		return (
			<AppBar
				position="fixed"
				color="primary"
				style={{ top: "auto", bottom: 0 }}
			>
				<BottomNavigation
					value={BottomNavStore.currentVal}
					onChange={BottomNavStore.handleChange}
					showLabels
				>
					<BottomNavigationAction
						onClick={() => {
							this.props.history.replace(
								`${Routes.MAIN.path}${Routes.HOME.path}`
							);
						}}
						label="Home"
						icon={<ion-icon name="home" />}
					/>
					<BottomNavigationAction
						onClick={() => {
							this.props.history.replace(
								`${Routes.MAIN.path}${Routes.USER_LIST.path}`
							);
						}}
						label="User List"
						icon={<ion-icon name="contacts" />}
					/>
					<BottomNavigationAction
						onClick={() => {
							this.props.history.replace(
								`${Routes.MAIN.path}${Routes.CONTRACTS_LIST.path}`
							);
						}}
						label="Contracts List"
						icon={<ion-icon name="document" />}
					/>
				</BottomNavigation>
			</AppBar>
		);
	}
}

export default BottomNav;
