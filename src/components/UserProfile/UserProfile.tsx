import { History } from "history";
import _ from "lodash";
import { inject, observer } from "mobx-react";
import React, { PureComponent, SFC } from "react";
import { withRouter } from "react-router";
import { UserModel } from "../../models/User";
import { ContractsStoreType } from "../../stores/ContractsStore";
import { UserStore as UserStoreType } from "../../stores/UserStore";
import { buildCreateContractPath } from "../../utility/Routes";
import Button from "../Buttons/Button";
import Carousel from "../Carousel/Carousel";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import Typography from "@material-ui/core/Typography";

import "./UserProfile.scss";
import { CardContent } from "@material-ui/core";

export interface ReviewCarouselCardsProps {
	reviewer: string;
	text: string;
	rating: number;
}

const ReviewCarouselCard: SFC<ReviewCarouselCardsProps> = props => (
	<Card className="review-carousel-card">
		<CardActionArea>
			<CardContent>
				<Typography gutterBottom variant="h5" component="h2">
					{props.reviewer}
				</Typography>
				<Typography variant="body2" color="textSecondary" component="p">
					{props.text}
				</Typography>
				{props.rating}
			</CardContent>
		</CardActionArea>
	</Card>
);
interface UserProfileProps {
	imageUrl: string;
	name: string;
	artistName: string;
	associatedVenues: string;
	reviews: ReviewCarouselCardsProps[];
	createContract?: boolean;

	history?: History;

	contractsStore?: ContractsStoreType;
	userStore?: UserStoreType;
}

@inject("contractsStore", "userStore")
@(withRouter as any)
@observer
class UserProfile extends PureComponent<UserProfileProps> {
	openContractCreate = () => {
		if (!this.props.userStore || !this.props.contractsStore) {
			throw Error("Stores should exists here ");
		}
		if (!this.props.history) {
			throw Error("Stores should exists here ");
		}
		const recipient: UserModel | null = this.props.userStore.currentUserProfile;
		if (_.isNil(recipient)) return;
		this.props.history.push(buildCreateContractPath(recipient.id));
	};

	componentWillUnmount() {
		if (this.props.userStore) {
			this.props.userStore.resetProfile();
		}
	}

	renderContractButton() {
		if (!this.props.createContract) return;
		return (
			<Button onClick={this.openContractCreate} text={"Create Contract"} />
		);
	}
	render() {
		return (
			<div className="user-profile">
				<div className="user-profile__top">
					<div className="image">
						<img src={this.props.imageUrl} />
					</div>
					<h5 className="user-profile__name subtitle-1">{this.props.name}</h5>
					<h6 className="user-profile__name subtitle-2">
						{this.props.artistName}
					</h6>
				</div>
				<div className="user-profile__bottom">
					<Carousel
						className={"user-profile--carousel"}
						title={"Reviews"}
						cards={this.props.reviews.map(cardProps => (
							<ReviewCarouselCard {...cardProps} />
						))}
					/>
				</div>
				{this.renderContractButton()}
			</div>
		);
	}
}

export default UserProfile;
