import React, { SFC } from "react";
import "./Loading.scss";

const LoadingBars: SFC = props => (
	<div className="lds-facebook">
		<div />
		<div />
		<div />
	</div>
);

export default LoadingBars;
