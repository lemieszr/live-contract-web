import React, { PureComponent } from "react";
import "./Footer.scss";
import { Link } from "react-router-dom";
import Routes from "../../utility/Routes";

interface FooterProps {
	match: any;
}
class Footer extends PureComponent<FooterProps> {
	render() {
		return (
			<div className="footer">
				<Link
					to={`${Routes.MAIN.path}${Routes.USER_PROFILE.path}`}
					className="footer__item"
				>
					{/* <i className="icon ion-md-contact" /> */}
					Profile
				</Link>
				<Link
					to={`${Routes.MAIN.path}${Routes.USER_LIST.path}`}
					className="footer__item"
				>
					{/* <i className="icon ion-md-contact" />> */}
					User list
					<i className="icon ion-md-contact" />
				</Link>
				<Link
					to={`${Routes.MAIN.path}${Routes.CONTRACTS_LIST.path}`}
					className="footer__item"
				>
					{/* <i className="icon ion-md-contact" /> */}
					Contracts
				</Link>
			</div>
		);
	}
}

export default Footer;
