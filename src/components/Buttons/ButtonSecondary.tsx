import React, { SFC } from "react";
import classnames from "classnames";
import "./Button.scss";

interface ButtonProps {
	onClick?: () => void;
	text: string;
	className?: string;
}

const ButtonSecondary: SFC<ButtonProps> = (props: ButtonProps) => {
	const btnClass = classnames("btn btn--secondary", props.className);
	return (
		<button onClick={props.onClick} className={btnClass}>
			<span>{props.text}</span>
		</button>
	);
};

export default ButtonSecondary;
