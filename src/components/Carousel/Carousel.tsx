import React, { SFC, ReactNode } from "react";
import _ from "lodash";
import "./Carousel.scss";
import classnames from "classnames";
import Grid from "@material-ui/core/Grid";

export interface CarouselCardProps {
	text: string;
}
interface CarouseProps {
	title: string;
	cards: ReactNode[];
	className?: string;
}

const CarouselCard: SFC<CarouselCardProps> = props => (
	<div key={props.text} className="carousel-card">
		{props.text}
	</div>
);

const Carousel: SFC<CarouseProps> = props => (
	<div className={classnames("carousel", props.className)}>
		<h5 className="carousel__title">{props.title}</h5>
		<Grid container spacing={8}>
			{props.cards}
		</Grid>
	</div>
);

export { CarouselCard };
export default Carousel;
