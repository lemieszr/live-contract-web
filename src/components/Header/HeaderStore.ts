import { observable, action } from "mobx";
import agent from "../../agent";
import UserStore from "../../stores/UserStore";

class HeaderStore {
	@observable
	sideNavOpen: boolean = false;

	@action
	toggleSideNav = () => {
		this.sideNavOpen = !this.sideNavOpen;
	};

	@action
	signUserOut = () => {
		agent.auth.signOut();
		UserStore.updateCurrentLoggedInUser(null);
		this.sideNavOpen = false;
	};
}
export { HeaderStore as HeaderStoreType };
export default new HeaderStore();
