import React, { Component } from "react";
import "./Header.scss";
import { inject, observer } from "mobx-react";
import { HeaderStoreType } from "./HeaderStore";
import classnames from "classnames";
import { Link } from "react-router-dom";
import Routes from "../../utility/Routes";
import * as Sentry from "@sentry/browser";

interface HeaderProps {
	showHeader: boolean;
	headerStore?: HeaderStoreType;

	// history?: History;
}

@inject("headerStore")
@observer
class Header extends Component<HeaderProps> {
	render() {
		if (!this.props.showHeader) {
			return null;
		}
		const { headerStore } = this.props;

		if (!headerStore) {
			return null;
		}

		const sideNavClass = classnames("side-nav", {
			"side-nav--open": headerStore.sideNavOpen
		});
		const sideNavBgClass = classnames("side-nav__bg", {
			"side-nav__bg--open": headerStore.sideNavOpen
		});

		return (
			<div className="header">
				<div className="header__menu-icon">
					<img
						onClick={headerStore.toggleSideNav}
						className="header__img-icon"
						src={require("./menu.svg")}
					/>
				</div>
				<Link to={Routes.HOME.path} replace className="header__menu-icon">
					<img className="header__img-icon" src={require("./home.svg")} />
				</Link>
				{/* <div
					className="header__menu-icon"
					onClick={() => {
						Sentry.captureMessage("User Report");
						Sentry.showReportDialog();
					}}
				>
					<img className="header__img-icon" src={require("./help.svg")} />
				</div> */}
				<div className={sideNavBgClass} onClick={headerStore.toggleSideNav} />
				<nav className={sideNavClass}>
					<div
						className="header__menu-icon"
						onClick={headerStore.toggleSideNav}
					>
						<img className="header__img-icon" src={require("./back.svg")} />
						<div className="button-text">Close</div>
					</div>
					<ul className="side-nav__items">
						<Link
							onClick={headerStore.toggleSideNav}
							to={`${Routes.MAIN.path}${Routes.USER_LIST.path}`}
						>
							<li className="side-nav__item subtitle-1">User List</li>
						</Link>
						<Link
							onClick={headerStore.toggleSideNav}
							to={`${Routes.MAIN.path}${Routes.USER_PROFILE.path}`}
						>
							<li className="side-nav__item subtitle-1">User Profile</li>
						</Link>
						<Link
							onClick={headerStore.toggleSideNav}
							to={`${Routes.MAIN.path}${Routes.CONTRACTS_LIST.path}`}
						>
							<li className="side-nav__item subtitle-1">Contracts List</li>
						</Link>
						<li className="side-nav__item subtitle-1">
							<div onClick={headerStore.signUserOut}>Sign Out</div>
						</li>
					</ul>
				</nav>
			</div>
		);
	}
}

export default Header;
