import React, { SFC } from "react";
import { Redirect, Route } from "react-router";
import Routes from "../../utility/Routes";
import { UserModel } from "../../models/User";

export interface RoutePrivateProps {
	currentUser: UserModel | null;
	path: string;
	component?: any;
}

const RoutePrivate: SFC<RoutePrivateProps> = props => {
	const { currentUser, ...restProps } = props;
	if (!currentUser) {
		return <Redirect to={Routes.LOGIN.path} />;
	}
	if (
		!currentUser.isProfileSetupComplete &&
		props.path !== Routes.PROFILE_CREATE.path
	) {
		return <Redirect to={Routes.PROFILE_CREATE.path} />;
	}

	return <Route {...restProps}>{props.children}</Route>;
};

export default RoutePrivate;
