import React, { SFC, ChangeEvent } from "react";
import "./FormComponents.scss";

const TextArea: SFC<{
	id: string;
	placeHolderText: string;
	className?: string;
	value: string;
	onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
}> = props => (
	<div className="form-field">
		<div className="form-field__control">
			<textarea
				id={props.id}
				className="form-field__textarea"
				placeholder=""
				value={props.value}
				onChange={props.onChange}
			/>
			<label htmlFor={props.id} className="form-field__label">
				{props.placeHolderText}
			</label>
			<div className="form-field__bar" />
		</div>
	</div>
);

export default TextArea;
