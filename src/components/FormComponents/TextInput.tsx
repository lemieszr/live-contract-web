import React, { SFC, ChangeEvent } from "react";
import classnames from "classnames";
import "./FormComponents.scss";

const TextInput: SFC<{
	id: string;
	placeHolderText: string;
	className?: string;
	type?: string;
	value?: string;
	required?: boolean;
	onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}> = props => (
	<div className={classnames("form-field", props.className)}>
		<div className="form-field__control">
			<input
				id={props.id}
				type={props.type || "text"}
				className="form-field__input"
				placeholder=" "
				required={props.required}
				value={props.value}
				onChange={props.onChange}
			/>
			<label htmlFor={props.id} className="form-field__label">
				{props.placeHolderText}
			</label>
			<div className="form-field__bar" />
		</div>
	</div>
);

export default TextInput;
