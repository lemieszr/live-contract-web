import React, { SFC, ChangeEvent, MouseEventHandler } from "react";
import "./FormComponents.scss";
import "./CheckBox.scss";

export const CheckBox: SFC<{
	id: string;
	label: string;
	className?: string;
	checked: boolean;
	onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}> = props => (
	<div className="form-field">
		<div className="md-checkbox">
			<input
				checked={props.checked}
				onChange={props.onChange}
				id={props.id}
				type="checkbox"
			/>
			<label htmlFor={props.id}>{props.label}</label>
		</div>
	</div>
);
