import React, { Component } from "react";
import Loading from "../Loading/Loading";
import "./LoadingContainer.scss";
const LoadingContainer = () => {
	return (
		<div className="loading-container">
			<div className="loading-container__cube">
				<ion-icon name="cube" />
			</div>
			<div className="loading-container__text heading-5">Loading</div>
		</div>
	);
};
export default LoadingContainer;
