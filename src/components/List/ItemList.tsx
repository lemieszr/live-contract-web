import React, { Component } from "react";
import _ from "lodash";
import "./ItemList.scss";
import classnames from "classnames";
interface ItemListProps {
	title: string;
	renderItems: (data: any[]) => any;
	data: any[] | null;
	collapsible?: boolean;
}
declare global {
	namespace JSX {
		interface IntrinsicElements {
			"ion-icon": any;
		}
	}
}

interface State {
	open: boolean;
}
class ItemList extends Component<ItemListProps, State> {
	state = {
		open: true
	};

	renderCollapseIcon = () => {
		if (!this.props.collapsible || _.isEmpty(this.props.data)) return;

		const iconclass = classnames("item-list__collapse", {
			rotate: !this.state.open
		});
		return (
			<div className={iconclass}>
				<ion-icon name="arrow-dropdown-circle" />
			</div>
		);
	};

	render() {
		return (
			<div className="item-list">
				<div className="item-list__container">
					<div
						className="item-list__header"
						onClick={() =>
							this.setState(prevState => ({ open: !prevState.open }))
						}
					>
						<div className="overline item-list__title">{this.props.title}</div>
						{this.renderCollapseIcon()}
					</div>

					<div className="item-list__item-container ">
						{!_.isNil(this.props.data) && this.state.open
							? this.props.renderItems(this.props.data)
							: null}
					</div>
				</div>
			</div>
		);
	}
}

export default ItemList;
