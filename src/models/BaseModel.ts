export const toJSON = (object: any) => {
	let json: any = {};
	Object.keys(object).forEach(key => {
		const val = object[key];
		json[key] = object[key];
	});
	return json;
};
