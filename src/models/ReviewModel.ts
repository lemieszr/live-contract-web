export interface ReviewModel {
	reviewText: string;
	id: string;
	value: number;
	contractId?: string;
	fromUser: string;
	aboutUser: string;
	userId?: string;
}
