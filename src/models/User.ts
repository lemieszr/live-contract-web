interface _NonInitializedUser {
	id: string;
	uid: string;
	isProfileSetupComplete: boolean;
	contractRefs: any[];
	contracts: string[];
	reviews: string[];
}

export interface UserModel extends _NonInitializedUser {
	firstName: string;
	lastName: string;
	artistName: string;
	phoneNumber?: string;
	profileImageUrl?: string;
}

export class NonInitializedUser implements _NonInitializedUser {
	id: string;
	uid: string;
	isProfileSetupComplete: boolean;
	contractRefs: any[];
	contracts: string[];
	reviews: string[];
	constructor(uid: string) {
		this.id = uid;
		this.uid = uid;
		this.isProfileSetupComplete = false;
		this.contractRefs = [];
		this.contracts = [];
		this.reviews = [];
	}
}
