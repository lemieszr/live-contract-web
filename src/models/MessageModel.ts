export interface MessageModel {
	id: string;
	creatorId?: string;
	recipientId?: string;
	contractId?: string;

	messageText: string;
}
