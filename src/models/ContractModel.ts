export enum ContractState {
	DRAFT = "DRAFT",
	PROPOSAL = "PROPOSAL",
	REBUTTAL = "REBUTTAL",
	REJECTED = "REJECTED",
	ACCEPTED = "ACCEPTED",
	CANCELED = "CANCELED",
	FULFILLED = "FULFILLED",
	CLOSED = "CLOSED",
	EXPIRED = "EXPIRED",
	ERROR = "ERROR "
}

export interface ContractFormModel {
	artistName: string;
	compensation: string;
	date: string;
	rider: boolean;
	hotel: boolean;
	flight: boolean;
	notes: string;
	firstName: string;
	lastName: string;
	venue: string;
	venueAddress: string;
	eventName: string;
	setLength: string;

	setTime: string;
}

export interface ContractData extends ContractFormModel {
	created_at: string;
	updated_at: string;
}

export interface ContractModel {
	id: string;
	created_at: string;
	updated_at: string;
	creatorId: string;
	recipientId: string;
	creatorRef: any;
	recipientRef: any;

	state: ContractState;

	messages: string[];
	reviews: string[];

	contract_revision_map: { [id: string]: ContractData };

	stub: any;

	contract_data_ids: string[];
	latest_contract_data_id: string;
}
