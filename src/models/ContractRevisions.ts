import { ContractState } from "./ContractModel";

export default interface ContractRevisions {
	id: string;
	latest_contract_id: string;
	latest_contract_ref: any;
	revision_collections_ids: string[];
	created_at: string;
	updated_at: string;
	creatorId: string;
	recipientId: string;

	state: ContractState;

	messages: string[];
	reviews: string[];
}
