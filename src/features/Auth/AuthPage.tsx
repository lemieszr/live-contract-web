import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import {
	AuthStoreValue,
	AuthStore,
	AuthErrorCode
} from "../../stores/AuthStore";
import "./AuthPage.scss";
import Button from "../../components/Buttons/Button";
import Routes from "../../utility/Routes";
import LoadingBars from "../../components/Loading/Loading";
import { CommonStore } from "../../stores/CommonStore";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import TextInput from "../../components/FormComponents/TextInput";

export interface AuthPageProps {
	authStore: AuthStore;
	commonStore: CommonStore;
	history: any;

	match: any;
}

@(withRouter as any)
@inject("authStore", "commonStore")
@observer
class AuthPage extends Component<AuthPageProps> {
	componentWillMount() {
		if (this.props.commonStore.token) {
			this.props.history.replace(Routes.HOME.path);
		}
	}

	componentWillUnmount() {
		this.props.authStore.reset();
	}

	handleSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		switch (this.props.match.url) {
			case Routes.LOGIN.path:
				this.props.authStore.login().then(() => {
					this.props.history.replace(Routes.HOME.path);
				});
				break;
			case Routes.SIGNUP.path:
				this.props.authStore.signupNewUser().then(() => {
					this.props.history.replace(Routes.HOME.path);
				});
				break;
			default:
				break;
		}
	};

	handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) =>
		this.props.authStore.setEmail(e.target.value);
	handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) =>
		this.props.authStore.setPassword(e.target.value);
	handlePasswordConfirmChange = (e: React.ChangeEvent<HTMLInputElement>) =>
		this.props.authStore.setPasswordConfirm(e.target.value);

	renderError = () => {
		if (!this.props.authStore.errors) {
			return null;
		}
		let errorText: string = "";
		switch (this.props.authStore.errors) {
			case AuthErrorCode.EMAIL_TAKEN:
				errorText = "Email Already In Use";
				break;
			case AuthErrorCode.WRONG_PASSWORD:
				errorText = "Invalid Credentials try again";
				break;
			case AuthErrorCode.USER_NOT_FOUND:
				errorText = "Could not locate this account";
				break;
			case AuthErrorCode.PASSWORD_CONFIRM:
				errorText = "Passwords do not match";
				break;
			case AuthErrorCode.WEAK_PASSWORD:
				errorText = "Your password is too weak, please try again";
				break;
			case AuthErrorCode.INVALID_EMAIL:
				errorText = "Invalid email address is invalid ";
				break;
			case AuthErrorCode.USER_DISABLED:
				errorText =
					"This user account has been disabled, please contact an administrator";
				break;
			case AuthErrorCode.TOO_MANY_REQUESTS:
				errorText = "You are trying that too much, please wait and try again";
				break;
			case AuthErrorCode.NETWORK_REQUEST_FAILED:
				errorText = "Failed to connect to the network. Please try again";
				break;
			case AuthErrorCode.NOT_AUTHORIZED:
				errorText = "You are not authoried to do that";
			case AuthErrorCode.ACCOUNT_ALREADY_EXISTS_WITH_OTHER_CREDENTIALS:
				errorText =	"This account already exists with a different email and password";
				break;
			case AuthErrorCode.INVALID_API_KEY:
				errorText = "Invalid API key"
				break;
			default:
				errorText = "Something went wrong please try again";
		}

		if (this.props.authStore.errors) {
			return <div className="auth__invalid-text">{errorText}</div>;
		}
	};

	renderPasswordConfirm = (values: AuthStoreValue) => {
		if (this.props.match.url === Routes.SIGNUP.path) {
			return (
				<fieldset className="auth__form-group">
					<TextInput
						id="confirmPassword"
						placeHolderText="Confirm Password"
						type="password"
						value={values.passwordConfirm}
						onChange={this.handlePasswordConfirmChange}
					/>
				</fieldset>
			);
		}
	};

	renderButtonOrLoading = (isLoading: boolean) => {
		if (isLoading) {
			return <LoadingBars />;
		}
		return <Button text={this.isLoginPage() ? "Login" : "Sign Up"} />;
	};

	isLoginPage() {
		return this.props.match.url === Routes.LOGIN.path;
	}

	render() {
		const { values, errors, inProgress, invalidEmail } = this.props.authStore;
		return (
			<div className="auth-page">
				<form className="auth-form" onSubmit={this.handleSubmitForm}>
					<div className="auth-form__title subtitle-2">
						{this.isLoginPage() ? "Login" : "Signup"}
					</div>
					<fieldset className="auth__form-group">
						<TextInput
							id="email"
							placeHolderText="email"
							type="email"
							value={values.email}
							onChange={this.handleEmailChange}
						/>
					</fieldset>
					<fieldset className="auth__form-group">
						<TextInput
							id="password"
							placeHolderText="password"
							type="password"
							value={values.password}
							onChange={this.handlePasswordChange}
						/>
					</fieldset>
					{this.renderPasswordConfirm(values)}
					{this.renderError()}
					<div className="auth-page__btn-container">
						{this.renderButtonOrLoading(this.props.authStore.inProgress)}
					</div>
					<Link
						className="auth__sign-up subtitle-2"
						to={this.isLoginPage() ? Routes.SIGNUP.path : Routes.LOGIN.path}
						replace
					>
						{this.isLoginPage()
							? "Create New Account"
							: "Login To Existing Account"}
					</Link>
				</form>
			</div>
		);
	}
}

export default AuthPage;
