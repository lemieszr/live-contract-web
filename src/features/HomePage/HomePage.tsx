import React, { Component } from "react";
import UserProfile from "../../components/UserProfile/UserProfile";
import { withRouter } from "react-router";
import { UserStore } from "../../stores/UserStore";
import { inject, observer } from "mobx-react";
import BottomNavStore from "../../stores/BottomNavStore";

interface Props {
	userStore: UserStore;
}

@inject("userStore")
@(withRouter as any)
@observer
class HomePage extends Component<Props> {
	componentDidMount() {
		BottomNavStore.setValue(0);
		const { currentLoggedInUser } = this.props.userStore;
		if (!currentLoggedInUser) {
			return null;
		}
		const { userStore } = this.props;
		userStore.pullUserProfile(currentLoggedInUser.id);
	}

	render() {
		const { userStore } = this.props;
		const { currentLoggedInUser } = this.props.userStore;
		if (!currentLoggedInUser) {
			return null;
		}

		return (
			<UserProfile
				imageUrl={currentLoggedInUser.profileImageUrl || ""}
				name={`${currentLoggedInUser.firstName} ${
					currentLoggedInUser.lastName
				}`}
				artistName={`${currentLoggedInUser.artistName}`}
				associatedVenues={""}
				reviews={userStore.reviewCarouselCards}
			/>
		);
	}
}

export default HomePage;
