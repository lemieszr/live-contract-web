import TextField from "@material-ui/core/TextField";
import _ from "lodash";
import { inject, observer } from "mobx-react";
import React, { Component, SFC } from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import ItemList from "../../components/List/ItemList";
import { UserModel } from "../../models/User";
import { UserListStoreType } from "../../stores/UserListStore";
import "./UserListPage.scss";
import BottomNavStore from "../../stores/BottomNavStore";

interface UserListStoreProps {
	userListStore: UserListStoreType;
}

const UserListItem: SFC<{
	name: string;
	artistName: string;
	imageUrl?: string;
	id: string;
}> = props => {
	return (
		<Link to={`/user/${props.id}`} className="user-list-item">
			<div className="user-list-item__image-container">
				<img
					className="user-list-item__image"
					src={props.imageUrl}
					alt={"image"}
				/>
			</div>
			<div className="user-list-item__text-container">
				<div className="subtitle-1">{props.artistName}</div>
				<div className="subtitle-2">{props.name}</div>
			</div>
		</Link>
	);
};

@(withRouter as any)
@inject("userListStore")
@observer
class UserListPage extends Component<UserListStoreProps> {
	componentDidMount() {
		BottomNavStore.setValue(1);
		this.props.userListStore.getUsers();
		this.props.userListStore.resetSearchTerm();
	}

	renderItems = (users: UserModel[]) => {
		return _.map(users, user => {
			return (
				<UserListItem
					id={user.uid}
					key={user.uid}
					name={`${user.firstName} ${user.lastName}`}
					artistName={user.artistName}
					imageUrl={user.profileImageUrl}
				/>
			);
		});
	};

	render() {
		const { userListStore } = this.props;
		return (
			<div className="user-list-page">
				<TextField
					id="outlined-search"
					label="Search For User"
					type="search"
					margin="normal"
					variant="outlined"
					onChange={userListStore.onSearchTermChange}
				/>
				<ItemList
					title="Users"
					data={userListStore.filteredList}
					renderItems={this.renderItems}
				/>
			</div>
		);
	}
}

export default UserListPage;
