import React, { Component, FormEvent } from "react";
import "./ProfileCreatePage.scss";
import TextInput from "../../components/FormComponents/TextInput";
import Button from "../../components/Buttons/Button";
import { inject, observer } from "mobx-react";
import { ProfileCreateStoreType } from "../../stores/ProfileCreateStore";
import { withRouter } from "react-router";
import { History } from "history";
import Routes from "../../utility/Routes";
import LoadingBars from "../../components/Loading/Loading";
import { UserStoreType } from "../../stores/UserStore";

interface Props {
	profileCreateStore: ProfileCreateStoreType;
	userStore: UserStoreType;
	history: History;
}

@inject("profileCreateStore", "userStore")
@(withRouter as any)
@observer
class ProfileCreatePage extends Component<Props> {
	componentDidMount() {
		this.props.profileCreateStore.reset();
	}

	componentDidUpdate() {
		const currentUser = this.props.userStore.currentUser;
		if (!currentUser) return;

		if (currentUser.isProfileSetupComplete) {
			this.props.history.replace(Routes.HOME.path);
		}
	}

	updateFirstNameHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
		this.props.profileCreateStore.firstNameHandler(e.target.value);

	updateLastNameHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
		this.props.profileCreateStore.lastNameHandler(e.target.value);

	photoUrlHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		const fileList = e.target.files;
		if (fileList) {
			let file: File = fileList[0];
			this.props.profileCreateStore.photoUrlHandler(file);
		}
	};

	artistName = (e: React.ChangeEvent<HTMLInputElement>) =>
		this.props.profileCreateStore.artistName(e.target.value);

	submitCreateProfile = (e: any) => {
		e.preventDefault();
		this.props.profileCreateStore.updateUserProfile().then(() => {
			this.props.history.replace(`${Routes.MAIN.path}`);
		});
	};

	render() {
		const { profileCreateStore } = this.props;
		return (
			<div className="profile-create-page">
				<form
					className="create-profile__form "
					onSubmit={this.submitCreateProfile}
				>
					<h6>{"Create your new profile"}</h6>
					<fieldset>
						<div className="row">
							<TextInput
								id="firstName"
								placeHolderText="First Name"
								type="text"
								required
								value={profileCreateStore.values.firstName}
								onChange={this.updateFirstNameHandler}
							/>
							<TextInput
								id="lastName"
								placeHolderText="Last Name"
								type="text"
								required
								value={profileCreateStore.values.lastName}
								onChange={this.updateLastNameHandler}
							/>
						</div>
						<div className="row">
							<TextInput
								id="photoUrl"
								placeHolderText="Photo Url (optional)"
								type="file"
								required
								onChange={this.photoUrlHandler}
							/>
						</div>
						<div className="row">
							<TextInput
								id="artistName"
								placeHolderText="Artist Name"
								type="text"
								value={profileCreateStore.values.artistName}
								onChange={this.artistName}
							/>
						</div>

						<div className="contract-page__button-container">
							{!profileCreateStore.isLoading ? (
								<Button text="Create Profile" />
							) : (
								<LoadingBars />
							)}
						</div>
					</fieldset>
				</form>
			</div>
		);
	}
}

export default ProfileCreatePage;
