import React, { Component, FormEvent, ChangeEvent } from "react";
import { ContractFormModel } from "../../models/ContractModel";
import TextInput from "../../components/FormComponents/TextInput";
import TextArea from "../../components/FormComponents/TextArea";
import { CheckBox } from "../../components/FormComponents/CheckBox";
import LoadingBars from "../../components/Loading/Loading";
import Button from "../../components/Buttons/Button";
import ButtonSecondary from "../../components/Buttons/ButtonSecondary";
import { observer } from "mobx-react";

enum StateValues {
	firstName = "firstName",
	lastName = "lastName",
	artistName = "artistName",
	compensation = "compensation",
	date = "date",
	notes = "notes",
	flight = "flight",
	rider = "rider",
	hotel = "hotel",
	venue = "venue",
	venueAddress = "venueAddress",
	setTime = "setTime",
	setLength = "setLength",
	eventName = "eventName"
}

interface CreateContractEditProps {
	contractFormModel: ContractFormModel;
	handleValueChange: (
		e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
	) => void;
	handleCheckBoxChange: (e: ChangeEvent<HTMLInputElement>) => void;
	createHandler: () => void;
	cancelHandler: () => void;
	submitText?: string;
	isLoading: boolean;
}

@observer
class ContractEditForm extends Component<CreateContractEditProps> {
	constructor(props: CreateContractEditProps) {
		super(props);
		this.state = {
			firstName: "",
			lastName: "",
			artistName: "",
			compensation: "",
			date: "",
			notes: "",
			venue: "",
			venueAddress: "",
			setTime: "",
			eventName: "",

			flight: false,
			rider: false,
			hotel: false
		};
	}

	render() {
		const { contractFormModel } = this.props;
		return (
			<form
				className="contract-form"
				onSubmit={(e: FormEvent<HTMLFormElement>) => {
					e.preventDefault();
				}}
			>
				<h6 className="create-contract-page__header">{`Offer For: ${
					contractFormModel.artistName
				}`}</h6>
				<fieldset>
					<div className="contract-page-row ">
						<TextInput
							className="form-field--contract-page"
							id={StateValues.firstName}
							placeHolderText="First Name"
							value={contractFormModel[StateValues.firstName]}
							onChange={this.props.handleValueChange}
						/>
						<TextInput
							className="form-field--contract-page"
							id={StateValues.lastName}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.lastName]}
							placeHolderText="Last Name"
						/>
					</div>
					<div className="contract-page-row">
						<TextInput
							id={StateValues.compensation}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.compensation]}
							placeHolderText="Compensation"
						/>
					</div>

					<div className="contract-page-row">
						<TextInput
							id={StateValues.venue}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.venue]}
							placeHolderText="Venue Name"
						/>
					</div>
					<div className="contract-page-row">
						<TextInput
							id={StateValues.venueAddress}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.venueAddress]}
							placeHolderText="Venue Address"
						/>
					</div>

					<div className="contract-page-row">
						<TextInput
							id={StateValues.date}
							type="date"
							value={contractFormModel[StateValues.date]}
							onChange={this.props.handleValueChange}
							placeHolderText="Date"
						/>
					</div>

					<div className="contract-page-row">
						<TextInput
							id={StateValues.setTime}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.setTime]}
							type="time"
							placeHolderText="Set Time"
						/>
					</div>

					<div className="contract-page-row">
						<TextInput
							id={StateValues.setLength}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.setLength]}
							type="time"
							placeHolderText="Set Length"
						/>
					</div>

					<div className="contract-page-row">
						<TextInput
							id={StateValues.eventName}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.eventName]}
							placeHolderText="EventName"
						/>
					</div>

					<div className="contract-page-row">
						<TextArea
							id={StateValues.notes}
							onChange={this.props.handleValueChange}
							value={contractFormModel[StateValues.notes]}
							placeHolderText="Additional Notes"
						/>
					</div>
				</fieldset>

				<div className="contract-page__button-container">
					{this.props.isLoading ? (
						<LoadingBars />
					) : (
						<Button
							onClick={this.props.createHandler}
							text={
								this.props.submitText
									? this.props.submitText
									: "Create Contract"
							}
						/>
					)}
					<ButtonSecondary onClick={this.props.cancelHandler} text="Cancel" />
				</div>
			</form>
		);
	}
}
export default ContractEditForm;
