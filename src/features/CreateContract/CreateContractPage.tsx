import React, {
	Component,
	SFC,
	ChangeEvent,
	FormEvent,
	MouseEventHandler
} from "react";
import _ from "lodash";
import "./CreateContract.scss";
import classnames from "classnames";
import Button from "../../components/Buttons/Button";
import ButtonSecondary from "../../components/Buttons/ButtonSecondary";
import { inject, observer } from "mobx-react";
import { ContractsStoreType } from "../../stores/ContractsStore";
import { UserStore as UserStoreType } from "../../stores/UserStore";
import { UserModel } from "../../models/User";
import { withRouter, match } from "react-router";
import { ContractFormModel } from "../../models/ContractModel";
import LoadingBars from "../../components/Loading/Loading";
import TextInput from "../../components/FormComponents/TextInput";
import TextArea from "../../components/FormComponents/TextArea";
import { CheckBox } from "../../components/FormComponents/CheckBox";
import classNames from "classnames";
import Routes, { buildContractDetailPath } from "../../utility/Routes";
import ContractEditForm from "./ContractEditForm";

interface CreateContractPageProps {
	contractsStore: ContractsStoreType;
	userStore: UserStoreType;

	history?: any;
	match?: match<any>;
}

@inject("contractsStore", "userStore")
@(withRouter as any)
@observer
class CreateContractPage extends Component<CreateContractPageProps> {
	componentDidMount() {
		if (!this.props.match || !this.props.userStore) return;
		const currentUser = this.props.userStore.currentLoggedInUser;
		if (!currentUser) {
			throw new Error("Attempting to create a contract with no logged in user");
		}
		const recipientId = this.props.match.params.id;
		this.props.contractsStore.loadCreateContractsPage(recipientId).then(() => {
			const recpient = this.props.contractsStore.recipientModel;
			if (!recpient) return;

			this.setState({
				firstName: recpient.firstName,
				lastName: recpient.lastName
			});
		});

		this.props.contractsStore.setRecipient(recipientId);
		this.props.contractsStore.setCreator(currentUser.id);
	}

	componentWillUnmount() {
		this.props.contractsStore.resetCreateContract();
	}

	cancelHandler = () => {
		if (_.isNil(this.props.history)) return;
		this.props.contractsStore.resetCreateContract();
		this.props.history.goBack();
	};

	createContractHandler = () => {
		this.props.contractsStore.createContract();
	};

	handleValueChange = (
		e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
	) => {
		e.preventDefault();
		if (!e.target) return;
		let targetId = e.target.id;
		let targetValue = e.target.value;
		this.props.contractsStore.updateContractField(targetId, targetValue);
	};

	handleCheckboxChange = (e: ChangeEvent<HTMLInputElement>) => {
		if (!e.target) return;
		let targetId = e.target.id;
		let targetValue = e.target.checked;
		this.props.contractsStore.updateContractField(targetId, targetValue);
	};

	navigateToContractDetail = () => {
		this.props.history.replace(
			buildContractDetailPath(this.props.contractsStore.createdContractId)
		);
	};

	render() {
		if (this.props.contractsStore.loadingCreateContractPage) {
			return <LoadingBars />;
		}
		const recipient: UserModel | null = this.props.userStore.currentUser;
		if (!recipient) {
			throw Error("Tried to create contract without a loading recipient");
		}
		const { contractFormModel } = this.props.contractsStore;

		return (
			<React.Fragment>
				<div
					className={classNames("modal", {
						open: this.props.contractsStore.modalOpen
					})}
				>
					<div className="modal__body">
						<svg
							className="checkmark"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 512 512"
						>
							<path d="M170.718 216.482L141.6 245.6l93.6 93.6 208-208-29.118-29.118L235.2 279.918l-64.482-63.436zM422.4 256c0 91.518-74.883 166.4-166.4 166.4S89.6 347.518 89.6 256 164.482 89.6 256 89.6c15.6 0 31.2 2.082 45.764 6.241L334 63.6C310.082 53.2 284.082 48 256 48 141.6 48 48 141.6 48 256s93.6 208 208 208 208-93.6 208-208h-41.6z" />
						</svg>
						<div className="subtitle-2 create-contract__success-text">
							Contract Successfully Created
						</div>
						<Button
							onClick={this.navigateToContractDetail}
							text="Goto contract detail"
						/>
					</div>
				</div>
				<div className="create-contract-page">
					{
						<ContractEditForm
							contractFormModel={contractFormModel}
							createHandler={this.createContractHandler}
							cancelHandler={this.cancelHandler}
							handleCheckBoxChange={this.handleCheckboxChange}
							handleValueChange={this.handleValueChange}
							isLoading={this.props.contractsStore.creatingContract}
						/>
					}
				</div>
			</React.Fragment>
		);
	}
}

export default CreateContractPage;
