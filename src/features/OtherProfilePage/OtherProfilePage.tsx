import React, { Component } from "react";
import UserProfile from "../../components/UserProfile/UserProfile";
import { withRouter, match } from "react-router";
import { inject, observer } from "mobx-react";
import { UserStore } from "../../stores/UserStore";

interface OtherProfilePageProps {
	userStore: UserStore;
	match: match<any>;
}

@(withRouter as any)
@inject("userStore")
@observer
class OtherProfilePage extends Component<OtherProfilePageProps> {
	componentDidMount() {
		const userId = this.props.match.params["id"];
		this.props.userStore.pullUserProfile(userId);
	}
	render() {
		const currentUser = this.props.userStore.currentUserProfile;

		if (this.props.userStore.loadingUser || !currentUser) {
			return <div>Implement a loading spinner</div>;
		}
		return (
			<UserProfile
				artistName={currentUser.artistName}
				associatedVenues={""}
				imageUrl={currentUser.profileImageUrl || ""}
				name={`${currentUser.firstName} ${currentUser.lastName}`}
				reviews={this.props.userStore.reviewCarouselCards}
				createContract={true}
			/>
		);
	}
}
export default OtherProfilePage;
