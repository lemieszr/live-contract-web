import React, { Component } from "react";
import { ContractData } from "../../models/ContractModel";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";

interface Props {
	currentContractData: ContractData;
	creatorProfileLink: string;
	creatorName: string;
	revisionCount: number;
}
@observer
class ContractDetailView extends Component<Props> {
	render() {
		const { currentContractData, creatorProfileLink, creatorName } = this.props;
		return (
			<div className="contract-detail-page__contract">
				<div className="contract-field subtitle-1">
					<strong>Contract Creator: </strong>
					<Link to={creatorProfileLink}>{creatorName}</Link>
				</div>
				<div className="contract-field subtitle-1">
					<strong>Artist Name: </strong>
					{currentContractData.artistName}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Legal Name: </strong>
					{`${currentContractData.firstName} ${currentContractData.lastName}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Venue: </strong>
					{`${currentContractData.venue}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Compensation: </strong>
					{`${currentContractData.compensation}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Notes: </strong>
					{`${currentContractData.notes}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Venue: </strong>
					{`${currentContractData.venue}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Venue Address: </strong>
					{`${currentContractData.venueAddress}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Event Name: </strong>
					{`${currentContractData.eventName}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Set Time: </strong>
					{`${currentContractData.setTime}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Date: </strong>
					{`${currentContractData.date}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Rider: </strong>
					{`${currentContractData.rider}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Hotel: </strong>
					{`${currentContractData.hotel}`}
				</div>
				<div className="contract-field subtitle-1">
					<strong>Flight: </strong>
					{`${currentContractData.flight}`}
				</div>
				<div className="subtitle-1 contract-field__revision-count">
					{`Contract Revision Number ${this.props.revisionCount}`}
				</div>
			</div>
		);
	}
}

export default ContractDetailView;
