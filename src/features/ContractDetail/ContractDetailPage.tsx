import { inject, observer } from "mobx-react";
import React, { Component } from "react";
import { match, withRouter } from "react-router-dom";
import agent from "../../agent";
import Button from "../../components/Buttons/Button";
import ButtonSecondary from "../../components/Buttons/ButtonSecondary";
import { ContractState } from "../../models/ContractModel";
import { ContractsStoreType } from "../../stores/ContractsStore";
import { ReviewEditStoreType } from "../../stores/ReviewEditStore";
import ContractDetailEditView from "./ContractDetailEditView";
import "./ContractDetailPage.scss";
import ContractDetailView from "./ContractDetailView";
import ContractMessages from "./ContractMessages";
import ContractReviewWidget from "./ContractReviewWidget";

interface ContractDetailPageProps {
	contractsStore?: ContractsStoreType;

	reviewEditStore?: ReviewEditStoreType;

	match?: match<any>;

	history: any;
}

interface State {
	editing: boolean;
}

@(withRouter as any)
@inject("contractsStore", "reviewEditStore")
@observer
class ContractDetailPage extends Component<ContractDetailPageProps, State> {
	state = {
		editing: false
	};

	private getStoreIfExists = (): ContractsStoreType => {
		if (
			!this.props.contractsStore ||
			!this.props.contractsStore.detailPageCurrentContract
		) {
			throw Error("No store");
		}
		return this.props.contractsStore;
	};

	fetchContract = () => {
		if (!this.props.contractsStore) {
			throw new Error(
				"Wtf should never react this point unless some issue with mobx"
			);
		}

		if (!this.props.match) {
			throw new Error("Could not find params for loading detail pages");
		}

		const contractId = this.props.match.params.id;

		this.props.contractsStore.getContractForDetailPage(contractId);
	};

	private unsubscribe: () => void = () => {};
	componentDidMount() {
		this.fetchContract();
		if (!this.props.match) return;
		const contractId = this.props.match.params.id;

		this.unsubscribe = agent.listeners.contract.attachListenerForContractId(
			contractId,
			() => {
				this.fetchContract();
			}
		);
	}

	componentWillUnmount() {
		this.unsubscribe();
	}

	acceptContract = () => {
		this.getStoreIfExists().updateContractStatus(ContractState.ACCEPTED);
	};

	rejectContract = () => {
		const contractStore = this.getStoreIfExists();
		contractStore.updateContractStatus(ContractState.REJECTED);
	};

	markContractfullfilled = () => {
		const contractStore = this.getStoreIfExists();
		contractStore.updateContractStatus(ContractState.FULFILLED);
	};

	cancelContract = () => {
		const contractStore = this.getStoreIfExists();
		contractStore.updateContractStatus(ContractState.CANCELED);
	};

	renderButtons = () => {
		const {
			detailPageCurrentContract,
			isCreator,
			contractExpired
		} = this.getStoreIfExists();
		if (!detailPageCurrentContract) {
			return null;
		}

		if (isCreator) {
			switch (detailPageCurrentContract.state) {
				case ContractState.REBUTTAL:
				case ContractState.PROPOSAL:
					return (
						<React.Fragment>
							<Button
								onClick={() => this.setState({ editing: true })}
								text="Edit"
							/>
							<ButtonSecondary
								onClick={this.cancelContract}
								text="Cancel Contract"
							/>
						</React.Fragment>
					);
				case ContractState.ACCEPTED:
					return (
						<React.Fragment>
							{contractExpired ? (
								<Button
									onClick={this.markContractfullfilled}
									text="Contract Fulfilled"
								/>
							) : null}
						</React.Fragment>
					);
				case ContractState.REJECTED:
					return null;
			}
		}

		switch (detailPageCurrentContract.state) {
			case ContractState.REBUTTAL:
			case ContractState.PROPOSAL:
				return (
					<React.Fragment>
						<Button onClick={this.acceptContract} text="Accept" />
						<ButtonSecondary onClick={this.rejectContract} text="Reject" />
					</React.Fragment>
				);
			case ContractState.ACCEPTED:
			case ContractState.REJECTED:
				return null;
		}
	};

	renderIfCreator() {
		const {
			detailPageCurrentContract,
			isCreator,
			contractExpired
		} = this.getStoreIfExists();
		if (
			!detailPageCurrentContract ||
			!isCreator ||
			detailPageCurrentContract.state !== ContractState.PROPOSAL
		) {
			return null;
		}

		return (
			<div className="contract-detail-page__status head">
				<div className="heading-5">{"Waiting on response"}</div>>
			</div>
		);
	}

	renderReview() {
		const {
			contractExpired,
			detailPageCurrentContract
		} = this.getStoreIfExists();
		if (!detailPageCurrentContract) return;
		if (
			contractExpired === true &&
			detailPageCurrentContract.state === ContractState.CLOSED
		) {
			return (
				<ContractReviewWidget
					contractId={detailPageCurrentContract.id}
					reviewEditStore={
						this.props.reviewEditStore || new ReviewEditStoreType()
					}
				/>
			);
		}
	}

	renderDetailOrEditForm = () => {
		const {
			detailPageCurrentContract,
			revisionCount,
			detailPageCurrentContractData
		} = this.getStoreIfExists();
		if (!detailPageCurrentContract || !detailPageCurrentContractData)
			return null;

		return this.state.editing ? (
			<ContractDetailEditView
				onSubmit={this.fetchContract}
				currentContractModel={detailPageCurrentContractData}
				contractId={detailPageCurrentContract.id}
				cancelHandler={() => this.setState({ editing: false })}
			/>
		) : (
			<ContractDetailView
				creatorName={this.getStoreIfExists().creatorName}
				creatorProfileLink={this.getStoreIfExists().creatorProfileLink}
				currentContractData={detailPageCurrentContractData}
				revisionCount={revisionCount}
			/>
		);
	};

	render() {
		if (
			!this.props.contractsStore ||
			!this.props.contractsStore.detailPageCurrentContract
		) {
			return null;
		}

		const currentContractData = this.props.contractsStore
			.detailPageCurrentContractData;
		const currentContract = this.props.contractsStore.detailPageCurrentContract;

		if (!currentContractData) {
			return null;
		}

		return (
			<div key={currentContract.id} className="contract-detail-page">
				{this.renderDetailOrEditForm()}
				<div className="contract-detail-page__status head">
					<div className="heading-5">{currentContract.state}</div>>
				</div>
				{this.renderIfCreator()}
				<div className="contract-detail-page__button-container">
					{this.renderButtons()}
				</div>
				<ContractMessages
					key={currentContract.id}
					contractDetailId={currentContract.id}
				/>

				{this.renderReview()}
			</div>
		);
	}
}

export default ContractDetailPage;
