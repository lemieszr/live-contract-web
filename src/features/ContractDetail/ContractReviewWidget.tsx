import React, { Component, ChangeEvent } from "react";
import TextArea from "../../components/FormComponents/TextArea";
import Rating from "react-rating";
import "./ContractReviewWidget.scss";
import { ReviewEditStoreType } from "../../stores/ReviewEditStore";
import Button from "../../components/Buttons/Button";
import { observer, inject } from "mobx-react";

interface Props {
	reviewEditStore: ReviewEditStoreType;
	contractId: string;
}

@observer
class ContractReviewWidget extends Component<Props> {
	componentDidMount() {
		this.props.reviewEditStore.initReview(this.props.contractId);
	}

	componentWillUnmount() {
		this.props.reviewEditStore.reset();
	}

	textAreaOnChange = (e: ChangeEvent<any>) =>
		this.props.reviewEditStore.setReviewText(e.target.value);

	widgetOrSubmited() {
		if (this.props.reviewEditStore.reviewSubmitted) {
			return (
				<div className="subtitle-1">Your Review has already been submitted</div>
			);
		} else {
			return (
				<React.Fragment>
					<div className="subtitle-1">
						How was your experience with this artist
					</div>
					<TextArea
						id="review"
						placeHolderText="How was your experience with this artist"
						key="review"
						value={this.props.reviewEditStore.reviewText}
						onChange={e =>
							this.props.reviewEditStore.setReviewText(e.target.value)
						}
					/>
					<Rating
						onChange={value => this.props.reviewEditStore.setRatingValue(value)}
					/>

					<Button
						text="Submit Review"
						onClick={this.props.reviewEditStore.submitReview}
					/>
				</React.Fragment>
			);
		}
	}

	render() {
		return (
			<div className="contract-review-widget">{this.widgetOrSubmited()}</div>
		);
	}
}

export default ContractReviewWidget;
