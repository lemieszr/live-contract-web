import React, { Component, ChangeEvent } from "react";
import ContractEditForm from "../CreateContract/ContractEditForm";
import { ContractEditStoreType } from "../../stores/ContractEditStore";
import { ContractData } from "../../models/ContractModel";
import { observer } from "mobx-react";

interface Props {
	contractId: string;
	currentContractModel: ContractData;
	onSubmit: () => void;
	cancelHandler: () => void;
}

@observer
class ContractDetailEditView extends Component<Props> {
	private contractEditStore: ContractEditStoreType = new ContractEditStoreType();

	componentDidMount() {
		this.contractEditStore.init(
			this.props.contractId,
			this.props.currentContractModel
		);
	}

	cancelHandler = () => {
		this.contractEditStore.reset();
		this.props.cancelHandler();
	};

	submitHandler = async () => {
		await this.contractEditStore.updateContract();
		this.cancelHandler();
	};

	handleCheckBoxChange = (e: ChangeEvent<HTMLInputElement>) => {
		if (!e.target) return;
		let targetId = e.target.id;
		let targetValue = e.target.checked;
		this.contractEditStore.updateContractField(targetId, targetValue);
	};
	handleValueChange = (
		e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
	) => {
		e.preventDefault();
		if (!e.target) return;
		let targetId = e.target.id;
		let targetValue = e.target.value;
		this.contractEditStore.updateContractField(targetId, targetValue);
	};

	render() {
		return (
			<ContractEditForm
				submitText="Update Contract"
				contractFormModel={this.contractEditStore.contractFormModel}
				cancelHandler={this.cancelHandler}
				isLoading={this.contractEditStore.updatingContract}
				createHandler={this.submitHandler}
				handleCheckBoxChange={this.handleCheckBoxChange}
				handleValueChange={this.handleValueChange}
			/>
		);
	}
}

export default ContractDetailEditView;
