import { inject, observer } from "mobx-react";
import React, { ChangeEvent } from "react";
import { MessageEditStoreType } from "../../stores/MessageEditStore";
import "./ContractMessages.scss";
import UserStore from "../../stores/UserStore";
const { Launcher } = require("react-chat-window");

interface InjectProps {
	messageEditStore?: MessageEditStoreType;
}

interface OwnProps {
	contractDetailId: string;
}

@inject("messageEditStore")
@observer
class ContractMessage extends React.Component<InjectProps & OwnProps> {
	getMessageStoreIfExists = () => {
		const { messageEditStore } = this.props;
		if (!messageEditStore) {
			throw Error("message store must exist at this point");
		}
		return messageEditStore;
	};

	componentDidMount() {
		this.getMessageStoreIfExists().reset();
		this.getMessageStoreIfExists().attachListener(this.props.contractDetailId);
		this.getMessageStoreIfExists().getMessagesById(this.props.contractDetailId);
	}

	componentWillUnmount() {
		this.getMessageStoreIfExists().reset();
	}

	public onMessageSent = (message: any) => {
		const messageEditStore = this.getMessageStoreIfExists();
		messageEditStore.createNewMessageForChatWindow(
			message,
			this.props.contractDetailId
		);
	};

	render() {
		const messageEditStore = this.getMessageStoreIfExists();

		const currentUser = UserStore.currentLoggedInUser;
		if (!currentUser) return;
		const currentUserId = currentUser.id;

		return (
			<Launcher
				agentProfile={{
					teamName: `Contract Chat`,
					imageUrl:
						"https://a.slack-edge.com/66f9/img/avatars-teams/ava_0001-34.png"
				}}
				onMessageWasSent={this.onMessageSent}
				messageList={messageEditStore.messagesForWidget}
				mute
				showEmoji={false}
			/>
		);
	}
}

export default ContractMessage;
