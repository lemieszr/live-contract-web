import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import _ from "lodash";
import { inject, observer } from "mobx-react";
import React, { Component, SFC } from "react";
import { Link } from "react-router-dom";
import ItemList from "../../components/List/ItemList";
import { ContractModel, ContractData } from "../../models/ContractModel";
import { UserModel } from "../../models/User";
import {
	ContractsStoreType,
	SortedContracts
} from "../../stores/ContractsStore";
import { UserStore as UserStoreType } from "../../stores/UserStore";
import { buildContractDetailPath } from "../../utility/Routes";
import "./ContractsListPage.scss";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import BottomNavStore from "../../stores/BottomNavStore";

interface ContractsListPageProps {
	contractsStore?: ContractsStoreType;
	userStore?: UserStoreType;
}

const ContractListItem: SFC<{
	artistName: string;
	date: string;
	contractId: string;
	linkTo: string;
}> = props => {
	return (
		<Link to={props.linkTo} className="contract-list-item">
			<div className="user-list-item__text-container">
				<div className="subtitle-1">{props.artistName}</div>
				<div className="subtitle-2">{props.date}</div>
				<div className="subtitle-2">{props.contractId}</div>
			</div>
		</Link>
	);
};

const ContractGridItemEmpty: SFC = () => (
	<Grid className="content-empty" item>
		<Typography variant="h4" component="h2">
			No Contract Data Found
		</Typography>
	</Grid>
);

const ContractGridItem: SFC<{ contractData: ContractData; link: string }> = ({
	contractData,
	link
}) => (
	<Grid className="contract-card-item" item>
		<Link to={link} className="contract-grid-item">
			<Card>
				<CardActionArea>
					<CardContent>
						<Typography gutterBottom variant="h4" component="h2">
							{contractData.date}
						</Typography>
						<Typography gutterBottom variant="h5" component="h2">
							{contractData.eventName}
						</Typography>
						<Typography variant="h5" color="textSecondary" component="h5">
							{contractData.artistName}
						</Typography>
					</CardContent>
				</CardActionArea>
			</Card>
		</Link>
	</Grid>
);

const ContractGrid: SFC<{ contractModels: ContractModel[] }> = ({
	contractModels
}) => {
	return (
		<Grid
			container
			spacing={16}
			style={{ marginTop: "16px" }}
			direction="column"
			justify="center"
			alignItems="center"
		>
			{contractModels.length === 0 ? (
				<ContractGridItemEmpty />
			) : (
				contractModels.map(model => {
					const contractData: ContractData =
						model.contract_revision_map[model.latest_contract_data_id];
					return (
						<ContractGridItem
							key={model.id}
							contractData={contractData}
							link={buildContractDetailPath(model.id)}
						/>
					);
				})
			)}
		</Grid>
	);
};

const intialState = {
	value: 0
};

@inject("contractsStore", "userStore")
@observer
class ContractsListPage extends Component<
	ContractsListPageProps,
	typeof intialState
> {
	state = intialState;
	componentWillMount() {
		BottomNavStore.setValue(2);
		if (!this.props.contractsStore || !this.props.userStore) {
			return;
		}
		const currentLoggedInUser: UserModel | null = this.props.userStore
			.currentLoggedInUser;
		if (!currentLoggedInUser) {
			throw new Error("Wtf should never get to this point");
		}
		this.props.contractsStore.pullContractsForCurrentUser(currentLoggedInUser);
	}

	renderContracts = (contracts: ContractModel[]) => {
		return _.map(contracts, (contract: ContractModel) => {
			const contractData =
				contract.contract_revision_map[contract.latest_contract_data_id];
			return (
				<ContractListItem
					key={contract.id}
					artistName={contractData.artistName}
					date={contractData.date}
					contractId={contract.id}
					linkTo={buildContractDetailPath(contract.id)}
				/>
			);
		});
	};

	handleChange = (event: any, newValue: any) => {
		this.setState({ value: newValue });
	};

	renderList(sortedContracts: SortedContracts) {
		switch (this.state.value) {
			case 0:
				return (
					<ContractGrid contractModels={sortedContracts.receivedContracts} />
				);
			case 1:
				return <ContractGrid contractModels={sortedContracts.sentContracts} />;
			case 2:
				return (
					<ContractGrid contractModels={sortedContracts.acceptedContracts} />
				);
			case 3:
				return (
					<ContractGrid contractModels={sortedContracts.rejectedContracts} />
				);
		}
	}

	render() {
		if (!this.props.contractsStore) return;
		const sortedContracts = this.props.contractsStore.sortedContracts;

		return (
			<div className="contract-list-page">
				<Tabs
					value={this.state.value}
					onChange={this.handleChange}
					indicatorColor="primary"
					textColor="primary"
					variant="fullWidth"
				>
					<Tab label="Received Contracts" />
					<Tab label="Sent Contracts" />
					<Tab label="Accepted Contracts" />
					<Tab label="Closed Contracts" />
				</Tabs>
				{this.renderList(sortedContracts)}
			</div>
		);
	}
}

export default ContractsListPage;
