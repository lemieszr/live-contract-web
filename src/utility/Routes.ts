import AuthPage from "../features/Auth/AuthPage";
import HomePage from "../features/HomePage/HomePage";
import UserListPage from "../features/UserListPage/UserListPage";
import ContractsListPage from "../features/ContractsListPage/ContractsListPage";
import OtherProfilePage from "../features/OtherProfilePage/OtherProfilePage";
import CreateContractPage from "../features/CreateContract/CreateContractPage";
import ContractDetailPage from "../features/ContractDetail/ContractDetailPage";
import ProfileCreatePage from "../features/ProfileCreatePage/ProfileCreatePage";

const routeId = ":id";

const Routes = {
	LOGIN: {
		component: AuthPage,
		path: "/login",
		exact: true
	},
	SIGNUP: {
		component: AuthPage,
		path: "/signup",
		exact: true
	},
	HOME: {
		component: HomePage,
		path: "/",
		exact: true
	},
	USER_PROFILE: {
		component: HomePage,
		path: "/profile/",
		exact: true
	},
	USER_LIST: {
		component: UserListPage,
		path: "/users/",
		exact: true
	},
	CONTRACTS_LIST: {
		component: ContractsListPage,
		path: "/contracts/",
		exact: true
	},
	OTHER_PROFILE: {
		component: OtherProfilePage,
		path: "/user/:id/",
		exact: true
	},
	CREATE_CONTRACT: {
		component: CreateContractPage,
		path: `/contract/create/${routeId}`,
		exact: true
	},
	CONTRACT_DETAIL: {
		component: ContractDetailPage,
		path: `/contract/detail/${routeId}/`,
		exact: true
	},
	PROFILE_CREATE: {
		component: ProfileCreatePage,
		path: `/profile/create/`,
		exact: true
	},
	MAIN: {
		path: "/main",
		exact: true
	}
};

export function buildProfilePath(uid: string) {
	return Routes.OTHER_PROFILE.path.replace(routeId, uid);
}

export function buildCreateContractPath(recieverId: string) {
	return Routes.CREATE_CONTRACT.path.replace(routeId, recieverId);
}

export function buildContractDetailPath(contractId: string) {
	return Routes.CONTRACT_DETAIL.path.replace(routeId, contractId);
}

export default Routes;
