import * as Sentry from "@sentry/browser";
// import { useStrict } from "mobx";
import { firestore, initializeApp } from "firebase";
import createBrowserHistory from "history/createBrowserHistory";
import { Provider } from "mobx-react";
import { RouterStore, syncHistoryWithStore } from "mobx-react-router";
import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import App from "./App";
import HeaderStore from "./components/Header/HeaderStore";
import * as serviceWorker from "./serviceWorker";
import AuthStore from "./stores/AuthStore";
import CommonStore from "./stores/CommonStore";
import ContractsStore from "./stores/ContractsStore";
import MessageEditStore from "./stores/MessageEditStore";
import ProfileCreateStore from "./stores/ProfileCreateStore";
import ReviewEditStore from "./stores/ReviewEditStore";
import UserListStore from "./stores/UserListStore";
import UserStore from "./stores/UserStore";
import "./styles/base.scss";
import BottomNavStore from "./stores/BottomNavStore";

Sentry.init({
	dsn: "https://3d86a77608324876ab4fefe590cd137b@sentry.io/1375637"
});

const browserHistory = createBrowserHistory();
const routingStore = new RouterStore();
export { routingStore as RouterStoreGlobal };
const stores = {
	authStore: AuthStore,
	userStore: UserStore,
	contractsStore: ContractsStore,
	userListStore: UserListStore,
	headerStore: HeaderStore,
	commonStore: CommonStore,
	profileCreateStore: ProfileCreateStore,
	messageEditStore: MessageEditStore,
	reviewEditStore: ReviewEditStore,
	bottomNavStore: BottomNavStore,

	routerStore: routingStore
};

// For easier debugging
(window as any)._____APP_STATE_____ = stores;

// promiseFinally.shim();
// useStrict(true);

// Initialize Firebase
var config = {
	apiKey: "AIzaSyDF_LXOZHmtiG5PDkPj-c7o2KRcAqTIU6w",
	authDomain: "offere-5ad35.firebaseapp.com",
	databaseURL: "https://offere-5ad35.firebaseio.com",
	projectId: "offere-5ad35",
	storageBucket: "offere-5ad35.appspot.com",
	messagingSenderId: "272446732563"
};
initializeApp(config);
firestore().settings({ timestampsInSnapshots: true });

const history = syncHistoryWithStore(browserHistory, routingStore);

// history.subscribe(location => {
// 	console.log(location);
// });

ReactDOM.render(
	<Provider {...stores}>
		<Router history={history}>
			<App />
		</Router>
	</Provider>,
	document.getElementById("root") || document.createElement("div") //div for testing purposes
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

export { routingStore as RouterStore };
